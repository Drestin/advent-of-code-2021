import scala.io.Source

case class Cuboid(fromX: Int, toX: Int, fromY: Int, toY: Int, fromZ: Int, toZ: Int) {
  def minus(other: Cuboid): Seq[Cuboid] = {
    if (!intersect(other)) {
      return Seq(this)
    }

    var parts = Seq.empty[Cuboid]
    if (fromX < other.fromX) {
      parts :+= Cuboid(fromX, other.fromX - 1, fromY, toY, fromZ, toZ)
    }
    if (toX > other.toX) {
      parts :+= Cuboid(other.toX + 1, toX, fromY, toY, fromZ, toZ)
    }
    val minX = Math.max(fromX, other.fromX)
    val maxX = Math.min(toX, other.toX)

    if (minX <= maxX) {
      if (fromY < other.fromY) {
        parts :+= Cuboid(minX, maxX, fromY, other.fromY - 1, fromZ, toZ)
      }
      if (toY > other.toY) {
        parts :+= Cuboid(minX, maxX, other.toY + 1, toY, fromZ, toZ)
      }
      val minY = Math.max(fromY, other.fromY)
      val maxY = Math.min(toY, other.toY)

      if (minY <= maxY) {
        if (fromZ < other.fromZ) {
          parts :+= Cuboid(minX, maxX, minY, maxY, fromZ, other.fromZ - 1)
        }
        if (toZ > other.toZ) {
          parts :+= Cuboid(minX, maxX, minY, maxY, other.toZ + 1, toZ)
        }
      }
    }

    parts
  }

  def volume: Long = (toX + 1L - fromX) * (toY + 1L - fromY) * (toZ + 1L - fromZ)

  def intersect(other: Cuboid): Boolean =
    fromX <= other.toX && toX >= other.fromX &&
    fromY <= other.toY && toY >= other.fromY &&
    fromZ <= other.toZ && toZ >= other.fromZ

  def validForPart1: Boolean =
    -50 <= fromX && fromX <= 50 &&
    -50 <= toX && toX <= 50 &&
    -50 <= fromY && fromY <= 50 &&
    -50 <= toY && toY <= 50 &&
    -50 <= fromZ && fromZ <= 50 &&
    -50 <= toZ && toZ <= 50
}

object Day22 {
  def main(args: Array[String]): Unit = {
    val input = Source.stdin.getLines().map(parse)//.filter(_._2.validForPart1)

    val cuboids = input.foldLeft(Seq.empty[Cuboid]) { (acc, elem) =>
      val (state, cuboid) = elem

      val newAcc = acc.flatMap(_.minus(cuboid))

      if (state) newAcc :+ cuboid else newAcc
    }
    println(cuboids.map(_.volume).sum)
  }

  def parse(line: String): (Boolean, Cuboid) = {
    val Array(stateStr, cuboidStr) = line.split(' ')
    val state = stateStr == "on"

    val Array(fromX, toX, fromY, toY, fromZ, toZ) = cuboidStr.split("((,?[xyz]=)|(\\.\\.))").tail.map(_.toInt)
    val cuboid = Cuboid(fromX, toX, fromY, toY, fromZ, toZ)

    (state, cuboid)
  }
}
