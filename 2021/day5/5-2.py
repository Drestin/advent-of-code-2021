import sys

lines = sys.stdin.read().splitlines()

points = dict()

for line in lines:
    [pt1, pt2] = line.split(' -> ')
    [x1, y1] = list(map(int, pt1.split(',')))
    [x2, y2] = list(map(int, pt2.split(',')))

    dx = x2 - x1
    dy = y2 - y1

    step_count = max(abs(dx), abs(dy))

    stepx = int(dx / step_count)
    stepy = int(dy / step_count)

    print(step_count, stepx, stepy)

    for i in range(step_count + 1):
        x = x1 + i * stepx
        y = y1 + i * stepy

        key = y * 1000 + x
        if key in points:
            points[key] += 1
        else:
            points[key] = 1

count = 0
for v in points.values():
    if v >= 2:
        count += 1

print(count)
