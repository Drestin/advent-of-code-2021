import sys

lines = sys.stdin.read().splitlines()

points = dict()

for line in lines:
    [pt1, pt2] = line.split(' -> ')
    [x1, y1] = list(map(int, pt1.split(',')))
    [x2, y2] = list(map(int, pt2.split(',')))

    if x1 == x2:
        for y in range(min(y1, y2), max(y1, y2) + 1):
            key = y * 1000 + x1
            if key in points:
                points[key] += 1
            else:
                points[key] = 1
    elif y1 == y2:
        for x in range(min(x1, x2), max(x1, x2) + 1):
            key = y1 * 1000 + x
            if key in points:
                points[key] += 1
            else:
                points[key] = 1

count = 0
for v in points.values():
    if v >= 2:
        count += 1

print(count)
