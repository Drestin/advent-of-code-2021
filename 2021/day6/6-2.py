fish = list(map(int, input().split(',')))

N = 256

# Calculated for 1 fish at 0 after x days
solutions = {
    174: 5238103,
    175: 5887913,
    176: 6249351,
    177: 6965599,
    178: 7510202,
    179: 8199372,
    180: 9037211,
    181: 9669228,
    182: 10821579,
}
for day in range(N):
    new_fish = 0
    stop = True

    for i in range(len(fish)):
        if fish[i] > 10:
            continue

        if N - day - fish[i] <= 182:
            fish[i] = solutions[N - day - fish[i]]

        else:
            stop = False
            fish[i] -= 1

            if fish[i] < 0:
                fish[i] = 6
                new_fish += 1

    fish += new_fish * [8]
    if stop:
        break

print(sum(fish))
