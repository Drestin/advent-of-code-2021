fish = list(map(int, input().split(',')))

for day in range(80):
    new_fish = 0
    for i in range(len(fish)):
        fish[i] -= 1

        if fish[i] < 0:
            fish[i] = 6
            new_fish += 1

    fish += new_fish * [8]

print(len(fish))
