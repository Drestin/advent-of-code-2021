import scala.collection.mutable

object Day12 extends App {
  type Cave = String
  def isBig(cave: Cave): Boolean = cave.charAt(0).isUpper

  type Edge = (Cave, Cave)

  val START: Cave = "start"
  val END: Cave = "end"

  val graph: mutable.Map[Cave, Seq[Cave]] = mutable.Map.empty


  case class ExplorationState(currentCave: Cave, exploredNodes: Map[Cave, Int], path: Seq[Cave], exploratedTwiceSmallCave: Boolean) {
    def maxExplorations(cave: Cave): Int = {
      if (cave == START || cave == END) {
        1
      } else if (isBig(cave)) {
        Int.MaxValue
      } else {
        if (exploratedTwiceSmallCave) 1 else 2
      }
    }

    def calculateNextStates(): Seq[ExplorationState] = {
      graph.getOrElse(currentCave, Seq.empty).flatMap { (nextCave: Cave) =>
        val nextNodeExploration = exploredNodes.getOrElse(nextCave, 0)
        if (nextNodeExploration < maxExplorations(nextCave) || isBig(nextCave)) {
          Some(ExplorationState(
            nextCave,
            exploredNodes.updated(nextCave, nextNodeExploration + 1),
            path.appended(nextCave),
            exploratedTwiceSmallCave || (!isBig(nextCave) && nextNodeExploration >= 1)
          ))
        } else {
          None
        }
      }
    }

    def calculatePathCountToEnd(): Int = {
      if (currentCave == END) {
//        println(path.mkString(","))
        1
      } else {
        calculateNextStates().map(_.calculatePathCountToEnd()).sum
      }
    }
  }

  val NEW_EXPLORATION: ExplorationState = ExplorationState(START, Map((START, 1)), Seq(START), false)

  Console.in.lines.forEach { line =>
    val Array(cave1, cave2) = line.split('-')

    graph.put(cave1, graph.getOrElse(cave1, Seq.empty).appended(cave2))
    graph.put(cave2, graph.getOrElse(cave2, Seq.empty).appended(cave1))
  }

  println(NEW_EXPLORATION.calculatePathCountToEnd())
}
