import sys

depth = 0
pos = 0

for line in sys.stdin.read().splitlines():
    line = line.split(' ')
    direction = line[0]
    distance = int(line[1])

    if direction == 'forward':
        pos += distance
    elif direction == 'down':
        depth += distance
    else:
        depth -= distance

print(pos * depth)
