import sys

depth = 0
pos = 0
aim = 0

for line in sys.stdin.read().splitlines():
    line = line.split(' ')
    direction = line[0]
    distance = int(line[1])

    if direction == 'forward':
        pos += distance
        depth += aim * distance
    elif direction == 'down':
        aim += distance
    else:
        aim -= distance

print(pos * depth)
