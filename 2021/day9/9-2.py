import sys

grid = list(map(lambda l: list(map(int, l)), sys.stdin.read().splitlines()))

width = len(grid[0])
heigth = len(grid)

ADJACENCIES = [(0, 1), (1, 0), (0, -1), (-1, 0)]


def get_adjacents(x, y):
    res = []
    for (dx, dy) in ADJACENCIES:
        adj_x = x + dx
        adj_y = y + dy

        if adj_x >= 0 and adj_y >= 0 and adj_x < width and adj_y < heigth:
            res.append((adj_x, adj_y))

    return res


bottom_points = []
for y in range(heigth):
    for x in range(width):
        is_bottom_point = True
        value = grid[y][x]

        for (adj_x, adj_y) in get_adjacents(x, y):
            adj_value = grid[adj_y][adj_x]
            if adj_value <= value:
                is_bottom_point = False

        if is_bottom_point:
            bottom_points.append((x, y))


def calculate_basin_size(point: (int, int)) -> int:
    size = 0

    greys = [point]
    blacks = set(greys)

    while len(greys) > 0:
        (x, y) = greys.pop()

        for adj in get_adjacents(x, y):
            (adj_x, adj_y) = adj

            if grid[adj_y][adj_x] < 9 and adj not in blacks:
                blacks.add(adj)
                greys.append(adj)

    return len(blacks)


sizes = list(map(calculate_basin_size, bottom_points))
sizes.sort(reverse=True)

print(sizes[0] * sizes[1] * sizes[2])
