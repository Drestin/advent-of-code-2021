import scala.io.Source

object Day14_1 {
  type Pair = (Char, Char)
  type Rules = Map[Pair, Char]
  type Polymer = Seq[Char]

  def runStep(rules: Rules, polymer: Polymer): Polymer = {
    val newPolymer = polymer.sliding(2).flatMap { pair =>
      if (pair.size < 2) return pair

      val a = pair(0)
      val b = pair(1)
      val newChar = rules.get((a, b))
      a +: newChar.toSeq
    }.toSeq :+ polymer.last

    newPolymer
  }

  def calculateScore(polymer: Polymer): Int = {
    val counts = polymer.groupBy(identity).view.map(_._2.size).toSeq.sorted

    counts.last - counts.head
  }

  def main(args: Array[String]): Unit = {
    val lines = Source.stdin.getLines().toSeq
    val template: Polymer = lines.head
    val rules: Rules = lines.drop(2).map { line =>
      val Array(pairStr, char) = line.split(" -> ")

      ((pairStr(0), pairStr(1)), char(0))
    }.toMap

    val process = Function.chain(Seq.fill(10)(runStep(rules, _)))

    println(calculateScore(process(template)))
  }
}
