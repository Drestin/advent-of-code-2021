import scala.io.Source

object Day14_2 {
  type Pair = (Char, Char)
  type Rules = Map[Pair, Char]
  type Polymer = Map[Pair, Long]

  def addToMapValue[T](map: Map[T, Long], key: T, value: Long): Map[T, Long] = {
    map.updated(key, map.getOrElse(key, 0L) + value)
  }

  def runStep(rules: Rules, polymer: Polymer): Polymer = {
    polymer.foldLeft(Map.empty[Pair, Long]) { (poly, v) =>
      val (pair, nb) = v
      rules.get(pair) match {
        case None => addToMapValue(poly, pair, nb)
        case Some(newChar) =>
          addToMapValue(addToMapValue(poly, (pair._1, newChar), nb), (newChar, pair._2), nb)
      }
    }
  }

  def calculateScore(polymer: Polymer, first: Char, last: Char): Long = {
    val counts = polymer.foldLeft(Map.empty[Char, Long]) { (counts, value) =>
      val (pair, nb) = value
      addToMapValue(addToMapValue(counts, pair._1, nb), pair._2, nb)
    }.map { value =>
      val (c, nb) = value
      val nbExtremities = (if (c == first) 1 else 0) + (if (c == last) 1 else 0)

      ((nb - nbExtremities) / 2) + nbExtremities
    }.toSeq.sorted

    counts.last - counts.head
  }

  def main(args: Array[String]): Unit = {
    val lines = Source.stdin.getLines().toSeq
    val template = lines.head
    val rules: Rules = lines.drop(2).map { line =>
      val Array(pairStr, char) = line.split(" -> ")

      ((pairStr(0), pairStr(1)), char(0))
    }.toMap

    var polymer = template.sliding(2).foldLeft(Map.empty[Pair, Long]) { (poly, str) =>
      addToMapValue(poly, (str(0), str(1)), 1L)
    }
    for (_ <- 1 to 40) {
      polymer = runStep(rules, polymer)
    }

    println(calculateScore(polymer, template.head, template.last))
  }
}
