import sys

last_value = 1000000000000000000
count = 0

for line in sys.stdin.read().splitlines():
    value = int(line)
    if last_value < value:
        count += 1
    last_value = value

print(count)
