import sys

count = 0
last_values = [int(input()), int(input()), int(input())]
last_sum = sum(last_values)

for line in sys.stdin.read().splitlines():
    last_values.append(int(line))

    last_values = last_values[1:]
    new_sum = sum(last_values)
    if new_sum > last_sum:
        count += 1
    last_sum = new_sum

print(count)
