

sealed trait Packet {
  val version: Int
}
case class LiteralPacket(version: Int, value: Long) extends Packet
case class OperatorPacket(version: Int, typeId: Int, subPackets: Seq[Packet]) extends Packet


object Day16 {
  val binaryString: String = Console.in.readLine().map { c =>
    val bits = Integer.parseInt(c.toString, 16).toBinaryString

    Seq.fill(4 - bits.length)('0').mkString + bits
  }.mkString
  var currentIndex: Int = 0

  def main(args: Array[String]): Unit = {
    val program = parseOnePacket()
//    println(program)
//    println(sumVersions(program))
    println(calculateValue(program))
  }

  def parsePackets(maxLength: Int = 100_000, maxCount: Int = 1000): Seq[Packet] = {
    var n = 0
    val startIndex = currentIndex
    var packets = Seq.empty[Packet]
    while (currentIndex < Math.min(binaryString.length, startIndex + maxLength) && n < maxCount) {
      packets :+= parseOnePacket()
      n += 1
    }

    packets
  }

  def parseOnePacket(): Packet = {
    val version = parseInt(3)
    val typeId = parseInt(3)

    if (typeId == 4) {
      LiteralPacket(version, parseLiteral())
    } else {
      val packets = if (parseInt(1) == 0) {
        parsePackets(maxLength = parseInt(15))
      } else {
        parsePackets(maxCount = parseInt(11))
      }
      OperatorPacket(version, typeId, packets)
    }
  }

  def parseInt(size: Int): Int = {
    val value = Integer.parseInt(binaryString.substring(currentIndex, currentIndex + size), 2)
    currentIndex += size

    value
  }

  def parseLiteral(): Long = {
    var literal = 0L
    while (true) {
      val block = parseInt(5)
      literal = (literal << 4) + (block & 15)

      if ((block & 16) == 0) {
        return literal
      }
    }
    ???
  }

  def sumVersions(packet: Packet): Int = packet match {
    case LiteralPacket(version, _) => version
    case OperatorPacket(version, _, subPackets) => version + subPackets.map(sumVersions).sum
  }

  def calculateValue(packet: Packet): Long = packet match {
    case LiteralPacket(_, value) => value
    case OperatorPacket(_, typeId, subPackets) => typeId match {
      case 0 => subPackets.map(calculateValue).sum
      case 1 => subPackets.map(calculateValue).product
      case 2 => subPackets.map(calculateValue).min
      case 3 => subPackets.map(calculateValue).max
      case 5 => if (calculateValue(subPackets(0)) > calculateValue(subPackets(1))) 1 else 0
      case 6 => if (calculateValue(subPackets(0)) < calculateValue(subPackets(1))) 1 else 0
      case 7 => if (calculateValue(subPackets(0)) == calculateValue(subPackets(1))) 1 else 0
      case _ => ???
    }
  }

}
