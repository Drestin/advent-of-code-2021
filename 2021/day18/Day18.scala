import scala.annotation.tailrec
import scala.io.Source

sealed trait SnailfishNumber
case class Leaf(value: Int) extends SnailfishNumber
case class Pair(left: SnailfishNumber, right: SnailfishNumber) extends SnailfishNumber

sealed trait Parent
/** Scanner comes from the left side of this parent */
case class Left(right: SnailfishNumber, parent: Option[Parent]) extends Parent
/** Scanner comes from the right side of this parent */
case class Right(left: SnailfishNumber, parent: Option[Parent]) extends Parent


case class Scanner(number: SnailfishNumber, parent: Option[Parent] = None) {
  def depth: Int = if (isTop) 0 else goUp.depth + 1
  def completeNumber: SnailfishNumber = goTop.number

  /** Return only if a changed was made */
  def findToSplit: Option[SnailfishNumber] = number match {
    case Leaf(value) => if (value > 9) {
      Some(Scanner(Pair(
        Leaf(value / 2),
        Leaf(value / 2 + (value % 2))
      ), parent).completeNumber)
    } else None
    case Pair(_, _) => goDownLeft.findToSplit.orElse(goDownRight.findToSplit)
  }

  /** Return only if a changed was made */
  def findToExplode: Option[SnailfishNumber] = number match {
    case Pair(Leaf(leftNumber), Leaf(rightNumber)) if this.depth >= 4 =>
      val tmp = this.withNumber(Leaf(0))
      val tmp2 = tmp.goNearestLeft match {
        case Some(scanner@Scanner(Leaf(n), _)) => scanner.withNumber(Leaf(n + leftNumber)).goNearestRight.get
        case None => tmp
        case other => throw new IllegalStateException(s"Should be a Leaf Scanner: $other")
      }
      val tmp3 = tmp2.goNearestRight match {
        case Some(scanner@Scanner(Leaf(n), _)) => scanner.withNumber(Leaf(n + rightNumber))
        case None => tmp2
        case other => throw new IllegalStateException(s"Should be a Leaf Scanner: $other")
      }
      Some(tmp3.completeNumber)

    case Leaf(_) => None
    case Pair(_, _) => goDownLeft.findToExplode.orElse(goDownRight.findToExplode)
  }

  def isTop: Boolean = parent.isEmpty
  def isLeaf: Boolean = number.isInstanceOf[Leaf]
  def isPair: Boolean = !isLeaf

  def goUp: Scanner = parent match {
    case Some(Left(right, parent)) => Scanner(Pair(number, right), parent)
    case Some(Right(left, parent)) => Scanner(Pair(left, number), parent)
    case None => throw new IllegalStateException("Trying to go up when at top.")
  }
  def goTop: Scanner = if (isTop) this else goUp.goTop

  def goDownLeft: Scanner = number match {
    case Pair(left, right) => Scanner(left, Some(Left(right, parent)))
    case Leaf(_) => throw new IllegalStateException("Trying to go down when at a leef.")
  }
  def goDownRight: Scanner = number match {
    case Pair(left, right) => Scanner(right, Some(Right(left, parent)))
    case Leaf(_) => throw new IllegalStateException("Trying to go down when at a leef.")
  }

  def withNumber(newNumber: SnailfishNumber): Scanner = this.copy(number = newNumber)

  def goNearestLeft: Option[Scanner] = parent match {
    case None => None
    case Some(Left(_, _)) => goUp.goNearestLeft
    case Some(Right(_, _)) => Some(goUp.goDownLeft.goDownMostRight)
  }
  def goNearestRight: Option[Scanner] = parent match {
    case None => None
    case Some(Right(_, _)) => goUp.goNearestRight
    case Some(Left(_, _)) => Some(goUp.goDownRight.goDownMostLeft)
  }
  def goDownMostRight: Scanner = if (isLeaf) this else goDownRight.goDownMostRight
  def goDownMostLeft: Scanner = if (isLeaf) this else goDownLeft.goDownMostLeft
}

object Day18 {
  def main(args: Array[String]): Unit = {
    val numbers = Source.stdin.getLines().map(parseSnailfishNumber(_)._1).toSeq
    // part 1
//    println(magnitude(numbers.reduceLeft((a, b) => reduce(add(a, b)))))

    println((for {
      a <- numbers
      b <- numbers
      if a != b
    } yield magnitude(reduce(add(a, b)))).max)
  }

  def parseSnailfishNumber(str: String): (SnailfishNumber, Int) = {
    val firstChar = str.head
    if (firstChar.isDigit) {
      (Leaf(firstChar.asDigit), 1)
    } else if (firstChar == '[') {
      val (left, usedCharsForLeft) = parseSnailfishNumber(str.substring(1))
      val (right, usedCharsForRight) = parseSnailfishNumber(str.substring(1 + usedCharsForLeft + 1))
      (Pair(left, right), 1 + usedCharsForLeft + 1 + usedCharsForRight + 1)
    } else {
      throw new IllegalStateException("Trying to parse " + str)
    }
  }

  def numberToString(number: SnailfishNumber): String = number match {
    case Leaf(value) => value.toString
    case Pair(left, right) => s"[${numberToString(left)},${numberToString(right)}]"
  }

  def magnitude(number: SnailfishNumber): Int = number match {
    case Leaf(value) => value
    case Pair(left, right) => 3 * magnitude(left) + 2 * magnitude(right)
  }

  def add(a: SnailfishNumber, b: SnailfishNumber): SnailfishNumber = Pair(a, b)

  @tailrec
  def reduce(number: SnailfishNumber): SnailfishNumber = {
//    println(numberToString(number))
    Scanner(number).findToExplode match {
      case Some(newNumber) => reduce(newNumber)
      case None => Scanner(number).findToSplit match {
        case Some(newNumber) => reduce(newNumber)
        case None => number
      }
    }
  }
}
