import sys


lines = sys.stdin.read().splitlines()
N = len(lines[0])


def find_rating(flip: bool) -> int:
    candidates = lines[:]

    for i in range(N):
        count = len(candidates)
        zero_counts = len([c for c in candidates if c[i] == '0'])

        wanted = '0'
        if zero_counts == count / 2:
            if not flip:
                wanted = '1'
        elif zero_counts > count / 2:
            if flip:
                wanted = '1'
        elif not flip:
            wanted = '1'

        candidates = [c for c in candidates if c[i] == wanted]

        if len(candidates) == 1:
            return int(candidates[0], 2)


gamma = find_rating(False)
epsilon = find_rating(True)

print(gamma * epsilon)
