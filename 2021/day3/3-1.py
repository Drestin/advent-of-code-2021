import sys


lines = sys.stdin.read().splitlines()
count = len(lines)
N = len(lines[0])

zero_counts = N * [0]

for line in lines:
    for i in range(N):
        if line[i] == '0':
            zero_counts[i] += 1

gamma_str = '0b'
epsilon_str = '0b'
for i in range(N):
    if zero_counts[i] > count / 2:
        gamma_str += '0'
        epsilon_str += '1'
    else:
        gamma_str += '1'
        epsilon_str += '0'

gamma = int(gamma_str, 2)
epsilon = int(epsilon_str, 2)
print(gamma * epsilon)
