import scala.io.Source

class Grid(val width: Int, val height: Int, val data: Array[Char]) {
  @inline
  def set(x: Int, y: Int, c: Char): Unit = {
    data((y % height) * width + (x % width)) = c
  }

  @inline
  def get(x: Int, y: Int): Char = data((y % height) * width + (x % width))

  def print(): Grid = {
    println(data.sliding(width, width).map(_.mkString).mkString("\n"))
    this
  }

  def copy: Grid = new Grid(width, height, Array.from(data))

  @inline
  def isEmpty(x: Int, y: Int): Boolean = get(x, y) == '.'

  def advance(): (Grid, Boolean) = {
    val grid2 = this.copy

    var moved = false
    // Horizontal
    for {
      y <- 0 until height
      x <- 0 until width
    } {
      val c = this.get(x, y)
      if (c == '>' && this.isEmpty(x + 1, y)) {
        grid2.set(x, y, '.')
        grid2.set(x + 1, y, '>')
        moved = true
      }
    }
    // Vertical
    val grid3 = grid2.copy
    for {
      y <- 0 until height
      x <- 0 until width
    } {
      val c = grid2.get(x, y)
      if (c == 'v' && grid2.isEmpty(x, y + 1)) {
        grid3.set(x, y, '.')
        grid3.set(x, y + 1, 'v')
        moved = true
      }
    }

    (grid3, moved)
  }
}

object Grid {
  def parseFromInput(): Grid = {
    val lines = Source.stdin.getLines().toSeq
    val width = lines.head.length
    val height = lines.size

    new Grid(width, height, lines.flatten.toArray)
  }
}

object Day25 {
  def main(args: Array[String]): Unit = {
    val stepCount = Iterator
      .iterate((Grid.parseFromInput(), true))(_._1.advance())
      .takeWhile(_._2)
      .size

    println(stepCount)
  }
}
