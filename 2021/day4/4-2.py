import sys
import re

BOARD_SIZE = 5
lines = sys.stdin.read().splitlines()

drawn_numbers: list[int] = list(map(int, lines[0].split(',')))
drawn_numbers.reverse()

boards = []

for i in range(2, len(lines), BOARD_SIZE + 1):
    boards.append(
        list(map(int, re.split(' +', ' '.join(lines[i:i+BOARD_SIZE]).strip()))))


def check_finished(board: list[int]) -> bool:
    for i in range(BOARD_SIZE):
        if sum(board[BOARD_SIZE*i:BOARD_SIZE*(i+1)]) == -5 or sum(board[i::BOARD_SIZE]) == -5:
            return True


def get_score(board: list[int]) -> int:
    return sum(filter(lambda x: x >= 0, board))


def cross_number(board: list[int], number: int) -> bool:
    for i in range(len(board)):
        if board[i] == number:
            board[i] = -1
            return True

    return False


def play_bingo():
    while True:
        number = drawn_numbers.pop()

        won_boards = []
        for board in boards:
            crossed = cross_number(board, number)
            if crossed:
                if check_finished(board):
                    if len(boards) == 1:
                        return number * get_score(board)
                    else:
                        won_boards.append(board)

        for board in won_boards:
            boards.remove(board)

        won_boards = []


print(play_bingo())
