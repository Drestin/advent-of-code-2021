import Image.{ImageData, Model, Pixel}

import scala.io.Source

class Image(val data: ImageData, val width: Int, val height: Int, val model: Model, val otherPixels: Pixel) {
  def getPixel(x: Int, y: Int): Pixel = {
    if (x < 0 || x >= width || y < 0 || y >= height) {
      otherPixels
    } else {
      data(y)(x)
    }
  }
  def setPixel(x: Int, y: Int, pixel: Pixel): Unit = data(y)(x) = pixel

  def enhance(): Image = {
    val newWidth = 2 + width
    val newHeight = 2 + height

    val newOtherPixels = if (otherPixels == 0) model(0) else model(0x1FF)

    val newData = Seq.fill(newHeight)(Array.fill(newWidth)(newOtherPixels))

    for {
      x <- 0 until newWidth
      y <- 0 until newHeight
    } {
      var index = 0
      for {
        dy <- -1 to 1
        dx <- -1 to 1
      } {
        index <<= 1
        index += this.getPixel(x - 1 + dx, y - 1 + dy)
      }
      newData(y)(x) = model(index)
    }

    new Image(newData, newWidth, newHeight, model, newOtherPixels)
  }

  def print(): Unit = {
    data.foreach(line => println(line.map(p => if (p == 0) '.' else '#').mkString))
  }

  def countWhitePixels: Int = {
    data.map(_.sum).sum
  }
}

object Image {
  type Pixel = Int
  type Model = Seq[Pixel]
  type ImageData = Seq[Array[Pixel]]

  def readFromInput(): Image = {
    val lines = Source.stdin.getLines().toSeq
    val model: Model = lines.head.map(c => if (c == '.') 0 else 1)
    val imageLines = lines.drop(2)
    val width = imageLines.head.length
    val height = imageLines.length

    val data: ImageData = Seq.fill(height)(Array.fill(width)(0))
    for {
      (line, y) <- imageLines.zipWithIndex
      (c, x) <- line.zipWithIndex
    } {
      val pixel = if (c == '.') 0 else 1
      data(y)(x) = pixel
    }

    new Image(data, width, height, model, otherPixels = 0)
  }
}

object Day20 {
  def main(args: Array[String]): Unit = {
    var image = Image.readFromInput()

    for (_ <- 1 to 50) {
      image = image.enhance()
    }

    println(image.countWhitePixels)
  }
}
