import sys

OPENING_CHARS = ['(', '[', '{', '<']
CLOSING_CHARS = [')', ']', '}', '>']

PAIRS = {'(': ')', '[': ']', '{': '}', '<': '>'}
SCORES = {')': 3, ']': 57, '}': 1197, '>': 25137}

lines = sys.stdin.read().splitlines()

score = 0
for l in lines:
    stack = []

    for c in l:
        if c in OPENING_CHARS:
            stack.append(c)
        else:
            opening = stack.pop()
            if PAIRS[opening] != c:
                score += SCORES[c]

print(score)
