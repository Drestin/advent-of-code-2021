import sys

OPENING_CHARS = ['(', '[', '{', '<']
CLOSING_CHARS = [')', ']', '}', '>']

PAIRS = {'(': ')', '[': ']', '{': '}', '<': '>'}
SCORES = {')': 1, ']': 2, '}': 3, '>': 4}

lines = sys.stdin.read().splitlines()

scores = []
for l in lines:
    corrupted = False
    stack = []
    score = 0

    for c in l:
        if c in OPENING_CHARS:
            stack.append(c)
        else:
            opening = stack.pop()
            if PAIRS[opening] != c:
                corrupted = True
                break

    if not corrupted:
        for c in stack[::-1]:
            score *= 5
            score += SCORES[PAIRS[c]]

        scores.append(score)

scores.sort()
print(scores[len(scores) // 2])
