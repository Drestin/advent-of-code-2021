using Distances
using Combinatorics

# Set of Matrix 3x3
function generate_rotations()
  rotations = Set()
  for x in range(0, 3)
    cos_x = round(Int, cos(x * 0.5pi))
    sin_x = round(Int, sin(x * 0.5pi))
    rot_x = [1 0 0; 0 cos_x -sin_x; 0 sin_x cos_x]
    for y in range(0, 3)
      cos_y = round(Int, cos(y * 0.5pi))
      sin_y = round(Int, sin(y * 0.5pi))
      rot_y = [cos_y 0 sin_y; 0 1 0; -sin_y 0 cos_y]
      for z in range(0, 3)
        cos_z = round(Int, cos(z * 0.5pi))
        sin_z = round(Int, sin(z * 0.5pi))
        rot_z = [cos_z -sin_z 0; sin_z cos_z 0; 0 0 1]

        rot = rot_x * rot_y * rot_z

        push!(rotations, rot)
      end
    end
  end
  rotations
end

# Array of Arrays of Positions
function parse_input()
  current_scanner = []
  scanners = []
  for l in readlines()
    if startswith(l, "---")
      current_scanner = []
    elseif l == ""
      push!(scanners, current_scanner)
    else
      pos = map(s -> parse(Int, s), split(l, ","))
      push!(current_scanner, pos)
    end
  end
  push!(scanners, current_scanner)
  scanners
end

# (Set of distances between beacons of this scanner, Map distance -> pair of indexes of beacons)
function calculate_beacon_distances(scanner)
  distances = Set()
  distances_to_pos = Dict()

  for (i, j) in combinations(range(1, length(scanner)), 2)
    pos1 = scanner[i]
    pos2 = scanner[j]
    d = euclidean(pos1, pos2)

    push!(distances, d)
    distances_to_pos[d] = (i, j)
  end

  (distances, distances_to_pos)
end

function calculate_beacon_deltas(scanner, with_neg = true)
  deltas = Set()
  deltas

  for (pos1, pos2) in combinations(scanner, 2)
    push!(deltas, pos2 - pos1)
    if with_neg
      push!(deltas, pos1 - pos2)
    end
  end

  deltas
end

# Array of scanner indexes with overlapping distances
function find_overlap_pairs(all_distances)
  overlaps = Dict()

  for (i, j) in combinations(range(1, length(all_distances)), 2)
    (d1, _) = all_distances[i]
    (d2, _) = all_distances[j]
    inter = intersect(d1, d2)
    # println(length(inter))
    if length(inter) > 60
      overlaps[(i, j)] = inter
      overlaps[(j, i)] = inter
    end
  end

  overlaps
end

# We know that those two scanners overlap.
# Return other as though basis is in the same basis than scanner zero
# Basis should already be reoriented and translated
function locate_scanner(basis_scanner, basis_distances_map, other_scanner, other_distances_map, common_distances, rotations)
  # Find rotation
  good_rotations = rotations
  for d in common_distances
    (bi, bj) = basis_distances_map[d]
    (oi, oj) = other_distances_map[d]

    basis_pos1 = basis_scanner[bi]
    basis_pos2 = basis_scanner[bj]
    other_pos1 = other_scanner[oi]
    other_pos2 = other_scanner[oj]

    old_good_rotations = good_rotations
    good_rotations = Set()

    for rot in old_good_rotations
      rotated_pos1 = rot * other_pos1
      rotated_pos2 = rot * other_pos2

      if (rotated_pos1 - basis_pos1 == rotated_pos2 - basis_pos2) || (rotated_pos1 - basis_pos2 == rotated_pos2 - basis_pos1)
        push!(good_rotations, rot)
      end
    end

    if length(good_rotations) == 1
      break
    end
  end

  rot = first(good_rotations)
  rotated_scanner = map(b -> rot * b, other_scanner)

  # Find translation
  d = first(common_distances)
  (bi, bj) = basis_distances_map[d]
  (oi, oj) = other_distances_map[d]

  basis_delta = basis_scanner[bj] - basis_scanner[bi]
  other_delta = rotated_scanner[oj] - rotated_scanner[oi]

  basis_pos = basis_scanner[bi]
  other_pos = nothing
  if basis_delta == other_delta
    other_pos = rotated_scanner[oi]
  elseif basis_delta == -other_delta
    other_pos = rotated_scanner[oj]
  else
    println("error ", basis_delta, ' ', other_delta)
  end

  translation = basis_pos - other_pos
  (translation, map(b -> translation + b, rotated_scanner))
end

function main()
  rotations = generate_rotations()
  scanners = parse_input()
  all_distances = map(calculate_beacon_distances, scanners)
  overlaps = find_overlap_pairs(all_distances)
  N = length(scanners)

  located_scanners = Set{Int}(1)
  scanner_positions = [[0, 0, 0]]

  # while length(located_scanners) < N
  for x in range(1, 1000)
    for i in located_scanners
      for j in range(1, N)
        if !(j in located_scanners) && (i, j) in keys(overlaps)
          # println((i, j))
          (scanner_pos, located_scanner) = locate_scanner(scanners[i], all_distances[i][2], scanners[j], all_distances[j][2], overlaps[(i, j)], rotations)
          scanners[j] = located_scanner
          push!(located_scanners, j)
          push!(scanner_positions, scanner_pos)
        end
      end
    end
  end

  # Part 1
  # all_beacons = Set()
  # for s in scanners
  #   for b in s
  #     push!(all_beacons, b)
  #   end
  # end
  # println(length(all_beacons))

  max_dist = 0
  for (a, b) in combinations(scanner_positions, 2)
    d = sum(abs.(a - b))
    if d > max_dist
      max_dist = d
    end
  end
  println(max_dist)

end

main()
