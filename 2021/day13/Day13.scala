import scala.io.Source

case class Point(x: Int, y: Int)
case class Fold(axis: Axis, coord: Int)

sealed trait Axis
case object X extends Axis
case object Y extends Axis

object Day13 {

  def foldPoints(points: Set[Point], fold: Fold): Set[Point] = {
    points.map { point =>
      if (fold.axis == X && point.x > fold.coord) {
        point.copy(x = 2 * fold.coord - point.x)
      } else if (fold.axis == Y && point.y > fold.coord) {
        point.copy(y = 2 * fold.coord - point.y)
      } else {
        point
      }
    }
  }

  def main(args: Array[String]): Unit = {
    val lines = Source.stdin.getLines()

    val points = lines.takeWhile(_ != "").map { line =>
      val Array(x, y) = line.split(',').map(_.toInt)
      Point(x,y)
    }.toSet

    val folds = lines.filter(_.startsWith("fold")).map { line =>
      val Array(axis, coord) = line.split(' ').last.split('=')
      Fold(if (axis == "x") X else Y, coord.toInt)
    }.toSeq

    val foldedPoints = folds.foldLeft(points)(foldPoints)

    val width = foldedPoints.maxBy(_.x).x
    val height = foldedPoints.maxBy(_.y).y

    for (y <- 0 to height) {
      for (x <- 0 to width) {
        val char = if (foldedPoints.contains(Point(x, y))) '#' else '.'
        print(char)
      }
      println()
    }
  }
}
