import math

[[x_min, x_max], [y_min, y_max]] = map(lambda s: map(int, s.split(
    '..')), map(lambda s: s.split('=')[-1], input().split(': ')[-1].split(', ')))


def tri_sum(n: int) -> int:
    return (n * (n + 1)) / 2


possible_start_xs = []

for start_x in range(math.ceil(math.sqrt(x_min)), x_max + 1):
    dx = start_x
    x = 0
    while dx > 0 and x <= x_max:
        x += dx
        dx -= 1
        if x_min <= x <= x_max:
            possible_start_xs.append(start_x)
            break


valid_initial_velocity_count = 0
for start_y in range(x_min, y_min - 1, -1):
    for start_x in possible_start_xs:
        x = 0
        y = 0
        dx = start_x
        dy = start_y
        while y >= y_min and x <= x_max:
            x += dx
            y += dy
            if dx > 0:
                dx -= 1
            dy -= 1

            if x_min <= x <= x_max and y_min <= y <= y_max:
                valid_initial_velocity_count += 1
                break
                #  Part 1:
                # print(start_y, tri_sum(start_y))
                # exit()

print(valid_initial_velocity_count)
