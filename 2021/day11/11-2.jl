const SIZE = 10

mutable struct Octopus
  energy::Int
  flashed::Bool
end

const Octopuses = Matrix{Octopus}
const Pos = Tuple{Int,Int}

const ADJACENCIES = [(-1, -1),
  (-1, 0),
  (-1, 1),
  (0, -1),
  (0, 1),
  (1, -1),
  (1, 0),
  (1, 1),
]

function curry(f, x)
  (xs...) -> f(x, xs...)
end

function parse_input()::Octopuses
  lines = readlines()

  octopuses = fill(Octopus(0, false), (SIZE, SIZE))
  for (y, l) in enumerate(lines)
    octopuses[:, y] = [Octopus(parse(Int, c), false) for c in l]
  end

  octopuses
end


function get_adjacents(x::Int, y::Int)::Vector{Pos}
  (ADJACENCIES
   |> curry(map, ((dx, dy)::Tuple{Int,Int}) -> (x + dx, y + dy))
   |> curry(filter, ((x, y)::Tuple{Int,Int}) -> x > 0 && x <= SIZE && y > 0 && y <= SIZE)
  )
end

function run(N::Int)
  octopuses = parse_input()

  for i in range(1, N)
    finished = false

    all_flashed = true
    for y in range(1, SIZE)
      for x in range(1, SIZE)
        octo = octopuses[x, y]
        if octo.flashed
          octo.energy = 0
          octo.flashed = false
        else
          all_flashed = false
        end

        octo.energy += 1
      end
    end

    if all_flashed
      return i - 1
    end

    while !finished
      finished = true
      for y in range(1, SIZE)
        for x in range(1, SIZE)
          octo = octopuses[x, y]

          if octo.energy > 9 && !octo.flashed
            finished = false
            octo.flashed = true

            for (xx, yy) in get_adjacents(x, y)
              octopuses[xx, yy].energy += 1
            end
          end
        end
      end
    end

  end

end

println(run(1000))
