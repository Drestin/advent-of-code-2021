import scala.io.Source
import scala.collection.mutable

sealed trait Term {
  def min: Long
  def max: Long
  /**
   * Return a simplified term (not doing the min max thing done in simplified).
   * We consider that input terms are simplified.
   */
  protected def preSimplified: Term

  def simplifiedInMod(mod: Long): Term

  def simplified: Term = {
    val simple = preSimplified

    val simpMax = simple.max
    val simpMin = simple.min

    if (simpMin == simpMax) {
      Number(simpMin)
    } else {
      simple
    }
  }

  def toString: String
}

case class Number(value: Long) extends Term {
  override val min: Long = value
  override val max: Long = value

  override protected val preSimplified: Term = this

  override def toString: String = value.toString

  override def simplifiedInMod(mod: Long): Term = Number(value % mod)
}
object Number {
  val Zero: Number = Number(0)
  val One: Number = Number(1)
}

case class Inp(index: Int) extends Term {
  override val min: Long = 1
  override val max: Long = 9

  override protected val preSimplified: Term = this

  override def toString: String = s"n_$index"

  override def simplifiedInMod(mod: Long): Term = this
}

case class Add(terms: Seq[Term]) extends Term {
  override val min: Long = terms.map(_.min).sum
  override val max: Long = terms.map(_.max).sum

  override protected def preSimplified: Term = {
    val (numbers: Seq[Number], others) = terms.partition({
      case Number(_) => true
      case _ => false
    })
    val numberSum = numbers.map(_.value).sum
    var needToResimplify = false
    val simpOthers = others.flatMap {
      case Add(terms) => needToResimplify = true; terms
      case t => Seq(t)
    }
    val simpTerms = if (numberSum == 0) simpOthers else Number(numberSum) +: simpOthers

    if (simpTerms.isEmpty) {
      Number.Zero
    } else if (simpTerms.lengthCompare(1) == 0) {
      simpTerms.head
    } else if (needToResimplify) {
      Add(simpTerms).preSimplified
    } else {
      Add(simpTerms)
    }
  }

  override def toString: String = "(" + terms.map(_.toString).mkString(" + ") + ")"

  override def simplifiedInMod(mod: Long): Term = Add(terms.map(_.simplifiedInMod(mod))).simplified
}

case class Mul(a: Term, b: Term) extends Term {
  override val min: Long = Seq(a.min * b.min, a.min * b.max, a.max * b.min, a.max * b.max).min
  override val max: Long = Seq(a.min * b.min, a.min * b.max, a.max * b.min, a.max * b.max).max

  override protected def preSimplified: Term = {
    val res = (a, b) match {
      case (Number(0), _) => Number.Zero
      case (Number(1), _) => b
      case (Number(n), Number(m)) => Number(n * m)
      case (_, Number(_)) => Mul(b, a).simplified
      case (Number(n), Mul(Number(m), c)) => Mul(Number(n * m), c).simplified
      case (_, Mul(Number(n), c)) => Mul(Number(n).simplified, Mul(a, c).simplified).simplified
//      case (_, Add(terms)) => Add(terms.map(Mul(a, _).simplified)).simplified
//      case (Add(terms), _) => Add(terms.map(Mul(_, b).simplified)).simplified
      case _ => this
    }

    res match {
      case Mul(Number(0), _) => print(this)
      case _ =>
    }
    res
  }

  override def toString: String = s"(${a.toString} * ${b.toString})"

  override def simplifiedInMod(mod: Long): Term = a match {
    case Number(n) if n % mod == 0 => Number.Zero
    case _ => Mul(a.simplifiedInMod(mod), b.simplifiedInMod(mod)).simplified
  }
}

case class Mod(a: Term, b: Term) extends Term {
  override val min: Long = 0
  override val max: Long = b.max - 1

  override protected def preSimplified: Term = {
    if (a.max < b.min) {
      a
    } else {
      b match {
        case Number(n) =>
          val s = a.simplifiedInMod(n)
          if (s.max < b.min) {
            s
          } else {
            Mod(Variable.newExpression(s), b)
          }
        case _ => this
      }
    }
  }


  override def toString: String = s"(${a.toString} % ${b.toString})"

  override def simplifiedInMod(mod: Long): Term = b match {
    case Number(n) if n % mod == 0 => a.simplifiedInMod(mod)
    case _ => this
  }
}

case class Div(a: Term, b: Term) extends Term {
  override val min: Long = Math.min(a.min / b.min, Math.min(a.max / b.max, Math.min(a.min / b.max, a.max / b.min)))
  override val max: Long = Math.max(a.min / b.min, Math.max(a.max / b.max, Math.max(a.min / b.max, a.max / b.min)))

  override protected val preSimplified: Term = {
    if (a == Number.Zero) {
      Number.Zero
    } else if (b == Number.One) {
      a
    } else {
      (a, b) match {
        case (Mul(Number(n), c), Number(m)) if n % m == 0 => Mul(Number(n / m), c).simplified
//        case (Add(terms), _) => Add(terms.map(t => Div(t, b).simplified)).simplified
        case _ => this
      }
    }
  }

  override def toString: String = s"(${a.toString} / ${b.toString})"

  override def simplifiedInMod(mod: Long): Term = this
}

case class Eql(a: Term, b: Term) extends Term {
  override val min: Long = 0
  override val max: Long = 1

  override protected val preSimplified: Term = {
    if (a.max < b.min || b.max < a.min) {
      Number.Zero
    } else if (a == b) {
      Number.One
    } else if (b == Number.Zero) {
      Variable.newExpression(this)
    } else {
      this
    }
  }

  override def toString: String = s"(${a.toString} == ${b.toString})"

  override def simplifiedInMod(mod: Long): Term = this
}

case class Variable(name: String, min: Long, max: Long) extends Term {
  override protected val preSimplified: Term = this

  override val toString: String = name

  override def simplifiedInMod(mod: Long): Term = this
}

object Variable {
  private val variableNames: Iterator[Char] = Iterator.iterate('A')(c => (c.toInt + 1).toChar)

  val expressions: mutable.Map[String, Term] = mutable.Map.empty

  def newExpression(exp: Term): Variable = {
    val varName = variableNames.next().toString
    expressions.put(varName, exp)
    Variable(varName, exp.min, exp.max)
  }

  def printExpressions(): Unit = {
    expressions.toSeq.sortBy(_._1).foreach { pair =>
      val (varName, exp) = pair
      println(s"$varName = $exp")
    }
  }
}

case class Registers(x: Term, y: Term, z: Term, w: Term, nextInput: Int) {
  def getReg(reg: String): Term = {
    reg match {
      case "x" => x
      case "y" => y
      case "z" => z
      case "w" => w
      case _ => throw new IllegalStateException("Illegal register: " + reg)
    }
  }

  def putReg(reg: String, term: Term): Registers = {
    reg match {
      case "x" => this.copy(x = term)
      case "y" => this.copy(y = term)
      case "z" => this.copy(z = term)
      case "w" => this.copy(w = term)
      case _ => throw new IllegalStateException("Illegal register: " + reg)
    }
  }
}

object Registers {
  val Init: Registers = Registers(Number.Zero, Number.Zero, Number.Zero, Number.Zero, 1)
}

object Day24 {
  def main(args: Array[String]): Unit = {
    val p = readProgram()
    Variable.printExpressions()
    println()
    println()
    println()
    println(p.toString)
    println(p.min, p.max)
  }

  def readProgram(): Term = {
    val reg = Source.stdin.getLines().take(300).zipWithIndex.foldLeft(Registers.Init) { (registers, pair) =>
      val (line, i) = pair

//      println(i, registers)
      if (line.startsWith("inp")) {
        val inpReg = line.split(' ').last
        val term = Inp(registers.nextInput)

        registers.putReg(inpReg, term).copy(nextInput = registers.nextInput + 1)
      } else {
        val Array(op, a, b) = line.split(' ')

        val termA = registers.getReg(a)
        val termB = b.toIntOption match {
          case Some(v) => Number(v)
          case None => registers.getReg(b)
        }

        val newTerm = op match {
          case "add" => Add(Seq(termA, termB))
          case "mul" => Mul(termA, termB)
          case "mod" => Mod(termA, termB)
          case "div" => Div(termA, termB)
          case "eql" => Eql(termA, termB)
          case _ => throw new IllegalStateException("Illegal operation: " + op)
        }
        registers.putReg(a, newTerm.simplified)
      }
    }

//    println(reg)
    reg.z
  }
}
