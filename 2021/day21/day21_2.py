# player_positions = (4, 8)  # example
player_positions = (8, 9)  # input

DIE_SIZE = 3
SPACE_COUNT = 10
VICTORY_SCORE = 21

# (pips, nb_universes)
DIES_RESULT = [(3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1)]

# Game instance : ([score1, score2], (pos1, pos2))


# POSITION_BITS = 4
# SCORE_BITS = 5
def state_to_key(positions, scores) -> int:
    return (positions[0] << 14) + (positions[1] << 10) + (scores[0] << 5) + scores[1]


def key_to_state(key: int):
    return ([(key >> 14) & 15, ((key >> 10) & 15)], [(key >> 5) & 31, key & 31])


def starting_universes():
    key = state_to_key(player_positions, (0, 0))
    return {key: 1}


def roll_die() -> int:
    global next_die_value, roll_count

    value = next_die_value
    roll_count += 1
    next_die_value += 1
    if next_die_value > DIE_SIZE:
        next_die_value = 1

    return value


def play(player_index: int):
    global player_positions, scores

    rolled = roll_die() + roll_die() + roll_die()
    new_pos = ((player_positions[player_index] + rolled - 1) % SPACE_COUNT) + 1
    player_positions[player_index] = new_pos
    scores[player_index] += new_pos


def main():
    universes = starting_universes()
    finished = False
    playing_player = 0
    won_universes = [0, 0]
    turns = 0
    while not finished:
        finished = True
        new_universes = {}
        turns += 1

        for (key, count) in universes.items():
            (positions, scores) = key_to_state(key)

            for (pips, nb_die_universes) in DIES_RESULT:
                new_pos = ((positions[playing_player] +
                            pips - 1) % SPACE_COUNT) + 1
                new_score = scores[playing_player] + new_pos
                total_universes = nb_die_universes * count

                if new_score >= VICTORY_SCORE:
                    won_universes[playing_player] += total_universes
                else:
                    finished = False
                    new_positions = positions.copy()
                    new_scores = scores.copy()
                    new_positions[playing_player] = new_pos
                    new_scores[playing_player] = new_score
                    new_key = state_to_key(new_positions, new_scores)

                    if new_key in new_universes:
                        new_universes[new_key] += total_universes
                    else:
                        new_universes[new_key] = total_universes

        playing_player = (playing_player + 1) % 2
        universes = new_universes

    (won0, won1) = won_universes

    print(won0 if won0 > won1 else won1)


main()
