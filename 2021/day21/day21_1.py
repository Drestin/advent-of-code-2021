# player_positions = [4, 8]  # example
player_positions = [8, 9]  # input

next_die_value = 1
roll_count = 0

DIE_SIZE = 100
SPACE_COUNT = 10

scores = [0, 0]


def roll_die() -> int:
    global next_die_value, roll_count

    value = next_die_value
    roll_count += 1
    next_die_value += 1
    if next_die_value > DIE_SIZE:
        next_die_value = 1

    return value


def play(player_index: int):
    global player_positions, scores

    rolled = roll_die() + roll_die() + roll_die()
    new_pos = ((player_positions[player_index] + rolled - 1) % SPACE_COUNT) + 1
    player_positions[player_index] = new_pos
    scores[player_index] += new_pos


while True:
    for i in range(len(player_positions)):
        play(i)
        if scores[i] >= 1000:
            print(roll_count * scores[i ^ 1])
            exit()
