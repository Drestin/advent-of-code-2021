import scala.collection.mutable
import scala.io.Source

case class Pos(x: Int, y: Int)

object Day15_1 {

  def main(args: Array[String]): Unit = {
    val riskLevels = Source.stdin.getLines().map(_.map(_.toString.toInt)).toIndexedSeq
    val width = riskLevels.head.size
    val height = riskLevels.size

    val dijkstraMap = Array.fill(height)(Array.fill(width)(Int.MaxValue))
    val queue = mutable.PriorityQueue[Pos]()(Ordering.by(pos => dijkstraMap(pos.x)(pos.y)))

    dijkstraMap(0)(0) = 0
    queue.enqueue(Pos(0,0))

    while (queue.nonEmpty) {
      val pos = queue.dequeue()
      val pathCost = dijkstraMap(pos.y)(pos.x)

      for ((dx, dy) <- Seq((-1, 0), (1, 0), (0, -1), (0, 1))) {
        val x = pos.x + dx
        val y = pos.y + dy

        if (x >= 0 && x < width && y >= 0 && y < height) {
          val newPathCost = pathCost + riskLevels(y)(x)

          if (dijkstraMap(y)(x) > newPathCost) {
            dijkstraMap(y)(x) = newPathCost
            queue.enqueue(Pos(x, y))
          }
        }
      }
    }

    println(dijkstraMap.last.last)

  }
}
