import scala.collection.mutable
import scala.io.Source

case class Pos(x: Int, y: Int)

object Day15_2 {

  def main(args: Array[String]): Unit = {
    val riskLevels = Source.stdin.getLines().map(_.map(_.toString.toInt)).toIndexedSeq

    val inputWidth = riskLevels.head.size
    val inputHeight = riskLevels.size

    def getRiskLevel(x: Int, y: Int): Int = {
      val base = riskLevels(x % inputWidth)(y % inputHeight)
      val value = base + Math.floorDiv(x, inputWidth) + Math.floorDiv(y, inputHeight)

      if (value <= 9) value else (value % 10) + 1
    }

    val width = inputWidth * 5
    val height = inputHeight * 5

    def distanceToEnd(x: Int, y: Int): Int = {
      width - 1 - x + (height - 1 - y)
    }

    val dijkstraMap = Array.fill(height)(Array.fill(width)(Int.MaxValue))
    val queue = mutable.PriorityQueue[Pos]()(Ordering.by(pos => dijkstraMap(pos.x)(pos.y)))

    dijkstraMap(0)(0) = 0
    queue.enqueue(Pos(0, 0))


      while (queue.nonEmpty) {
        val pos = queue.dequeue()
        val pathCost = dijkstraMap(pos.y)(pos.x)

        if (pathCost + distanceToEnd(pos.x, pos.y) < dijkstraMap.last.last) {
          for ((dx, dy) <- Seq((-1, 0), (1, 0), (0, -1), (0, 1))) {
            val x = pos.x + dx
            val y = pos.y + dy

            if (x >= 0 && x < width && y >= 0 && y < height) {
              val newPathCost = pathCost + getRiskLevel(x, y)

              if (dijkstraMap(y)(x) > newPathCost) {
                dijkstraMap(y)(x) = newPathCost

                if (x == width - 1 && y == height - 1) {
                  println(dijkstraMap.last.last)
                } else {
                  queue.addOne(Pos(x, y))
                }
              }
            }
          }
        }

    }

    println(dijkstraMap.last.last)

  }
}
