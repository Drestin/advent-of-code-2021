import statistics

positions = list(map(int, input().split(',')))

median = statistics.median(positions)

used_fuel = sum(map(lambda p: abs(p - median), positions))

print(used_fuel)
