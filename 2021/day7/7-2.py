positions = list(map(int, input().split(',')))

min_used_fuel = 10000000000


def triangle_number(n: int) -> int:
    return int(n * (n + 1) / 2)


for line_pos in range(min(positions), max(positions)+1):
    used_fuel = sum(
        map(lambda p: triangle_number(abs(line_pos - p)), positions))

    if used_fuel < min_used_fuel:
        min_used_fuel = used_fuel

print(min_used_fuel)
