import sys

lines = sys.stdin.read().splitlines()

result = 0
for line in lines:
    [models_str, display_str] = line.split(' | ')

    models = models_str.split(' ')
    models.sort(key=lambda a: len(a))

    models = list(map(set, models))

    known_models = {1: models[0], 7: models[1], 4: models[2], 8: models[9]}

    for m in models[6:9]:
        if not m.issuperset(known_models[7]):
            known_models[6] = m
        elif m.issuperset(known_models[4]):
            known_models[9] = m
        else:
            known_models[0] = m

    for m in models[3:6]:
        if m.issuperset(known_models[7]):
            known_models[3] = m
        elif m.issubset(known_models[6]):
            known_models[5] = m
        else:
            known_models[2] = m

    number = 0
    for dd in display_str.split(' '):
        model = set(dd)

        for (digit, m) in known_models.items():
            if m == model:
                number = number * 10 + digit
                break

    result += number

print(result)
