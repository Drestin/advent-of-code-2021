import sys

lines = sys.stdin.read().splitlines()

count = 0
for line in lines:
    [_, display] = line.split(' | ')
    display_digits = display.split(' ')
    print(display_digits)

    for d in display_digits:
        if len(d) in [2, 3, 4, 7]:
            count += 1

print(count)
