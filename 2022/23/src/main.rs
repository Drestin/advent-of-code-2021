use std::{
    collections::{BTreeMap, BTreeSet},
    io,
    ops::Add,
};

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
struct Pos {
    x: i64,
    y: i64,
}

impl Add<Pos> for Pos {
    type Output = Pos;

    fn add(self, rhs: Pos) -> Self::Output {
        Pos {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Pos {
    fn around(&self) -> impl Iterator<Item = Pos> + '_ {
        Dir::West
            .deltas_with_diagonals()
            .chain(Dir::East.deltas_with_diagonals())
            .chain([Dir::North.delta(), Dir::South.delta()].into_iter())
            .map(|d| *self + d)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
enum Dir {
    North,
    South,
    West,
    East,
}

const ALL_DIR: [Dir; 4] = [Dir::North, Dir::South, Dir::West, Dir::East];

impl Dir {
    fn delta(&self) -> Pos {
        match self {
            Dir::North => Pos { x: 0, y: -1 },
            Dir::South => Pos { x: 0, y: 1 },
            Dir::West => Pos { x: -1, y: 0 },
            Dir::East => Pos { x: 1, y: 0 },
        }
    }

    fn deltas_with_diagonals(&self) -> impl Iterator<Item = Pos> {
        let middle = self.delta();

        if middle.x == 0 {
            [
                Pos { x: -1, y: middle.y },
                middle,
                Pos { x: 1, y: middle.y },
            ]
            .into_iter()
        } else {
            [
                Pos { x: middle.x, y: -1 },
                middle,
                Pos { x: middle.x, y: 1 },
            ]
            .into_iter()
        }
    }
}

fn caculate_result_part1(elves: &BTreeSet<Pos>) -> i64 {
    let x_min = elves.iter().map(|p| p.x).min().unwrap();
    let x_max = elves.iter().map(|p| p.x).max().unwrap();
    let y_min = elves.iter().map(|p| p.y).min().unwrap();
    let y_max = elves.iter().map(|p| p.y).max().unwrap();

    let rect_area = (x_max - x_min + 1) * (y_max - y_min + 1);

    rect_area - elves.len() as i64
}

fn move_elves(elves: &BTreeSet<Pos>, dir_order: &[Dir]) -> BTreeSet<Pos> {
    let mut new_elves: BTreeSet<Pos> = BTreeSet::new();
    // target -> [origins]
    let mut wanted_moves: BTreeMap<Pos, Vec<Pos>> = BTreeMap::new();

    for &elf in elves {
        if elf.around().all(|p| !elves.contains(&p)) {
            new_elves.insert(elf);
        } else {
            let dir = dir_order.iter().find(|d| {
                d.deltas_with_diagonals()
                    .all(|dt| !elves.contains(&(dt + elf)))
            });

            if let Some(dir) = dir {
                let new_pos = elf + dir.delta();

                wanted_moves.entry(new_pos).or_default().push(elf);
            } else {
                new_elves.insert(elf);
            }
        }
    }

    for (wanted_pos, wanting_elves) in wanted_moves.into_iter() {
        if wanting_elves.len() == 1 {
            new_elves.insert(wanted_pos);
        } else {
            new_elves.extend(wanting_elves);
        }
    }

    new_elves
}

fn print_elves(elves: &BTreeSet<Pos>) {
    let x_min = elves.iter().map(|p| p.x).min().unwrap();
    let x_max = elves.iter().map(|p| p.x).max().unwrap();
    let y_min = elves.iter().map(|p| p.y).min().unwrap();
    let y_max = elves.iter().map(|p| p.y).max().unwrap();

    for y in y_min..=y_max {
        for x in x_min..=x_max {
            let c = if elves.contains(&Pos { x, y }) {
                '#'
            } else {
                '.'
            };
            print!("{c}");
        }
        println!();
    }
    println!();
}

fn part1(elves: &BTreeSet<Pos>) -> i64 {
    let mut elves = elves.clone();
    let mut dir_order = ALL_DIR.clone();

    for _ in 0..10 {
        elves = move_elves(&elves, &dir_order);
        dir_order.rotate_left(1);
        // print_elves(&elves);
    }

    caculate_result_part1(&elves)
}

fn part2(elves: &BTreeSet<Pos>) -> usize {
    let mut elves = elves.clone();
    let mut dir_order = ALL_DIR.clone();

    let mut i = 1;
    loop {
        let new_elves = move_elves(&elves, &dir_order);

        if new_elves == elves {
            break;
        }

        elves = new_elves;
        dir_order.rotate_left(1);
        // print_elves(&elves);
        i += 1;
    }

    i
}

fn main() {
    let elves: BTreeSet<Pos> = io::stdin()
        .lines()
        .enumerate()
        .flat_map(|(y, l)| {
            l.unwrap()
                .chars()
                .enumerate()
                .filter_map(move |(x, c)| {
                    if c == '#' {
                        Some(Pos {
                            x: x as i64,
                            y: y as i64,
                        })
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>()
        })
        .collect();

    println!("Part 1: {:?}", part1(&elves));
    println!("Part 2: {:?}", part2(&elves));
}
