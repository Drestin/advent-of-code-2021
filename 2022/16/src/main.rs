use std::{
    cmp::Reverse,
    collections::{BTreeSet, HashMap, HashSet},
    hash::Hash,
    io,
    iter::repeat,
};

use nom::{
    branch::alt,
    bytes::streaming::tag,
    character::complete::{alpha1, u32},
    IResult,
};
use priority_queue::PriorityQueue;

const START_NAME: &str = "AA";

type Valve = String;

#[derive(Debug)]
struct Caves {
    tunnels: HashMap<Valve, HashMap<Valve, usize>>, // distances
    flow_rates: HashMap<Valve, usize>,
}

fn parse_line(input: &str) -> IResult<&str, (String, usize, Vec<String>)> {
    let (input, _) = tag("Valve ")(input)?;
    let (input, name) = alpha1(input)?;
    let (input, _) = tag(" has flow rate=")(input)?;
    let (input, flow_rate) = u32(input)?;
    let (input, _) = alt((
        tag("; tunnels lead to valves "),
        tag("; tunnel leads to valve "),
    ))(input)?;

    let tunnels: Vec<String> = input.split(", ").map(|t| t.to_string()).collect();

    Ok((
        "",
        (
            name.to_string(),
            flow_rate as usize,
            tunnels.iter().map(|t| t.to_string()).collect(),
        ),
    ))
}

fn parse_caves(lines: impl Iterator<Item = String>) -> Caves {
    let mut direct_tunnels: HashMap<Valve, Vec<Valve>> = HashMap::new();
    let mut flow_rates: HashMap<Valve, usize> = HashMap::new();

    lines
        .map(|l| parse_line(&l).unwrap().1)
        .for_each(|(name, flow_rate, my_tunnels)| {
            if flow_rate > 0 || name == START_NAME {
                flow_rates.insert(name.clone(), flow_rate);
            }
            let vec = direct_tunnels.entry(name).or_default();
            for adj_valve in my_tunnels {
                vec.push(adj_valve);
            }
        });

    let mut tunnels: HashMap<String, HashMap<String, usize>> = HashMap::new();

    for start_valve in flow_rates.keys().cloned() {
        let tunnel_entry = tunnels.entry(start_valve.clone()).or_default();

        let mut blacks: HashSet<Valve> = HashSet::new();
        let mut greys: PriorityQueue<Valve, Reverse<usize>> = PriorityQueue::new();

        greys.push(start_valve.clone(), Reverse(0));
        blacks.insert(start_valve.clone());

        while let Some((curr_valve, dist)) = greys.pop() {
            if flow_rates.contains_key(&curr_valve)
                && curr_valve != start_valve
                && !tunnel_entry.contains_key(&curr_valve)
            {
                tunnel_entry.insert(curr_valve.clone(), dist.0);
            }
            for v in &direct_tunnels[&curr_valve] {
                if !blacks.contains(v) {
                    blacks.insert(v.clone());
                    greys.push(v.clone(), Reverse(dist.0 + 1));
                }
            }
        }
    }

    flow_rates.remove(START_NAME);

    Caves {
        tunnels,
        flow_rates,
    }
}

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
enum ActorState {
    Finished,
    AtValve(Valve),
    MovingToOpen { valve: Valve, opening_turn: usize },
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct State {
    turn: usize,
    released_pressure: usize,
    actors: Vec<ActorState>,
    opened_valves: BTreeSet<Valve>,
    remaining_valves: BTreeSet<Valve>,
}

impl State {
    fn go_to_next_decision_point(&mut self, caves: &Caves, max_rounds: usize) {
        if self.actors.iter().all(|a| *a == ActorState::Finished) {
            self.go_to_end(caves, max_rounds);
            return;
        }

        let decision_turn: usize = self
            .actors
            .iter()
            .filter_map(|a| {
                if let ActorState::MovingToOpen {
                    valve: _,
                    opening_turn,
                } = a
                {
                    Some(*opening_turn)
                } else if let ActorState::AtValve(_) = a {
                    panic!("Should not be here");
                } else {
                    None
                }
            })
            .min()
            .unwrap();

        self.advance_time_to(caves, decision_turn);
        for i in 0..self.actors.len() {
            if let ActorState::MovingToOpen {
                valve,
                opening_turn,
            } = self.actors[i].clone()
            {
                if opening_turn == decision_turn {
                    self.open_valve(&valve);
                    self.actors[i] = ActorState::AtValve(valve.clone());
                }
            }
        }
    }

    fn open_valve(&mut self, valve: &Valve) {
        self.remaining_valves.remove(valve);
        self.opened_valves.insert(valve.clone());
    }

    fn go_to_end(&mut self, caves: &Caves, max_rounds: usize) {
        self.actors.fill(ActorState::Finished);
        self.advance_time_to(caves, max_rounds);
    }

    fn advance_time_to(&mut self, caves: &Caves, to_turn: usize) {
        self.released_pressure += self
            .opened_valves
            .iter()
            .map(|v| caves.flow_rates[v])
            .sum::<usize>()
            * (to_turn - self.turn);
        self.turn = to_turn;
    }

    fn possible_next_states(self, caves: &Caves, max_rounds: usize) -> Vec<State> {
        let mut next_states: Vec<State> = Vec::new();

        next_states.push(self.clone());

        for (i, actor) in self.actors.iter().enumerate() {
            let prev_states = next_states;
            let mut new_states = Vec::new();

            if let ActorState::AtValve(curr_valve) = actor.clone() {
                for target_valve in self.remaining_valves.iter() {
                    let opening_turn = self.turn + caves.tunnels[&curr_valve][target_valve] + 1;
                    if opening_turn < max_rounds {
                        let new_actor = ActorState::MovingToOpen {
                            valve: target_valve.clone(),
                            opening_turn,
                        };
                        for s in prev_states.iter() {
                            let mut new_state = s.clone();
                            new_state.actors[i] = new_actor.clone();
                            new_states.push(new_state);
                        }
                    }
                }
                if new_states.is_empty() {
                    new_states = prev_states
                        .into_iter()
                        .map(|mut s| {
                            s.actors[i] = ActorState::Finished;
                            s
                        })
                        .collect();
                }
            } else {
                new_states = prev_states;
            }
            next_states = new_states;
        }

        next_states
            .iter_mut()
            .for_each(|s| s.go_to_next_decision_point(caves, max_rounds));

        next_states
    }

    fn priority(&self, _caves: &Caves) -> usize {
        self.released_pressure / (self.turn + 1)
    }

    fn max_release_pressure(&self, caves: &Caves, max_rounds: usize) -> usize {
        self.released_pressure + caves.flow_rates.values().sum::<usize>() * (max_rounds - self.turn)
    }
}

fn simulate(caves: &Caves, max_rounds: usize, actor_count: usize) -> usize {
    let start_state = State {
        turn: 0,
        released_pressure: 0,
        opened_valves: BTreeSet::new(),
        remaining_valves: caves.flow_rates.keys().cloned().collect(),
        actors: repeat(ActorState::AtValve(START_NAME.to_string()))
            .take(actor_count)
            .collect(),
    };

    let mut states: PriorityQueue<State, usize> = PriorityQueue::new();
    let prio = start_state.priority(caves);
    states.push(start_state, prio);

    let mut best_released_pressure: usize = 0;

    let mut searched_count = 0;
    let mut cutted_count = 0;
    let mut end_seen_count = 0;

    while let Some((state, _)) = states.pop() {
        searched_count += 1;
        if searched_count % 100_000 == 0 {
            println!("seen={searched_count} ; cutted={cutted_count} ; ends={end_seen_count} : current_best={best_released_pressure}");
        }
        if state.released_pressure > best_released_pressure {
            best_released_pressure = state.released_pressure;
        }

        if state.turn < max_rounds {
            state
                .possible_next_states(caves, max_rounds)
                .into_iter()
                .for_each(|s| {
                    if s.max_release_pressure(caves, max_rounds) > best_released_pressure {
                        let prio = s.priority(caves);
                        states.push(s, prio);
                    } else {
                        cutted_count += 1;
                    }
                })
        } else {
            end_seen_count += 1;
        }
    }

    best_released_pressure
}

fn part1(caves: &Caves) -> usize {
    simulate(caves, 30, 1)
}

fn part2(caves: &Caves) -> usize {
    simulate(caves, 26, 2)
}

fn main() {
    let caves = parse_caves(io::stdin().lines().map(|l| l.unwrap()));

    println!("Part 1: {}", part1(&caves));
    println!("Part 2: {}", part2(&caves));
}
