use std::{cmp::Ordering, io};

use nom::{
    branch::alt, bytes::complete::tag, character, multi::separated_list0, sequence::delimited,
    IResult,
};

#[derive(Debug, PartialEq, Eq, Clone)]
enum Paquet {
    Integer(u32),
    List(Vec<Paquet>),
}

impl PartialOrd for Paquet {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Paquet::Integer(a), Paquet::Integer(b)) => a.partial_cmp(b),
            (Paquet::Integer(_), Paquet::List(_)) => {
                Paquet::List(vec![self.clone()]).partial_cmp(other)
            }
            (Paquet::List(_), Paquet::Integer(_)) => {
                self.partial_cmp(&Paquet::List(vec![other.clone()]))
            }
            (Paquet::List(list_a), Paquet::List(list_b)) => {
                for i in 0..list_a.len().max(list_b.len()) {
                    let a = list_a.get(i);
                    let b = list_b.get(i);

                    match (a, b) {
                        (None, None) => panic!("Should not be here"),
                        (None, Some(_)) => return Some(Ordering::Less),
                        (Some(_), None) => return Some(Ordering::Greater),
                        (Some(p_a), Some(p_b)) => {
                            if let Some(ord) = p_a.partial_cmp(p_b) {
                                if ord != Ordering::Equal {
                                    return Some(ord);
                                }
                            }
                        }
                    }
                }
                Some(Ordering::Equal)
            }
        }
    }
}

impl Ord for Paquet {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

fn integer(s: &str) -> IResult<&str, Paquet> {
    character::complete::u32(s).map(|(s, n)| (s, Paquet::Integer(n)))
}

fn list(s: &str) -> IResult<&str, Paquet> {
    delimited(tag("["), separated_list0(tag(","), paquet), tag("]"))(s)
        .map(|(s, l)| (s, Paquet::List(l)))
}

fn paquet(s: &str) -> IResult<&str, Paquet> {
    alt((list, integer))(s)
}

fn part1(pairs: &[(Paquet, Paquet)]) -> usize {
    pairs
        .iter()
        .enumerate()
        .filter_map(|(i, (a, b))| if a <= b { Some(i + 1) } else { None })
        .sum()
}

fn part2(pairs: &[(Paquet, Paquet)]) -> usize {
    let div1 = Paquet::List(vec![Paquet::List(vec![Paquet::Integer(2)])]);
    let div2 = Paquet::List(vec![Paquet::List(vec![Paquet::Integer(6)])]);

    let mut paquets: Vec<&Paquet> = pairs
        .iter()
        .map(|(a, b)| vec![a, b].into_iter())
        .flatten()
        .collect::<Vec<_>>();
    paquets.push(&div1);
    paquets.push(&div2);

    paquets.sort();

    let i1 = paquets.iter().position(|p| **p == div1).unwrap() + 1;
    let i2 = paquets.iter().position(|p| **p == div2).unwrap() + 1;

    i1 * i2
}

fn main() {
    let input = io::stdin().lines().collect::<Result<Vec<_>, _>>().unwrap();

    let pairs: Vec<(Paquet, Paquet)> = input
        .split(|s| s == "")
        .map(|group| {
            let mut iter = group.iter().map(|p| paquet(&p).unwrap().1);
            (iter.next().unwrap(), iter.next().unwrap())
        })
        .collect::<Vec<_>>();

    println!("Part 1: {}", part1(&pairs));
    println!("Part 2: {}", part2(&pairs));
}
