use std::{collections::HashSet, io};

fn part1(input: &[Vec<u8>]) -> usize {
    input
        .iter()
        .map(|bag| {
            let half = bag.len() / 2;
            let (comp1, comp2) = (&bag[..half], &bag[half..]);

            for item in comp1 {
                if comp2.contains(item) {
                    return *item as usize;
                }
            }

            panic!("Failed to find a common item");
        })
        .sum()
}

fn part2(input: &[Vec<u8>]) -> usize {
    input
        .chunks(3)
        .map(|group| {
            let intersection = group
                .iter()
                .map(|bag| bag.iter().copied().collect::<HashSet<u8>>())
                .reduce(|bag1, bag2| bag1.intersection(&bag2).copied().collect::<HashSet<_>>())
                .unwrap()
                .into_iter()
                .collect::<Vec<_>>();

            if intersection.len() != 1 {
                panic!("Zero or more than one common object in group")
            }

            intersection[0] as usize
        })
        .sum()
}

fn main() {
    let input = io::stdin()
        .lines()
        .map(|line| {
            line.unwrap()
                .bytes()
                .map(|c| {
                    if c.is_ascii_lowercase() {
                        c - 96u8
                    } else if c.is_ascii_uppercase() {
                        c - 65u8 + 27u8
                    } else {
                        panic!("Failed to parse");
                    }
                })
                .collect()
        })
        .collect::<Vec<_>>();

    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
}
