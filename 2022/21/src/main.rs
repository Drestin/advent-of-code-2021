use std::{collections::HashMap, io};

type MonkeyId = String;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
enum Op {
    Add,
    Subtract,
    Multiply,
    Divide,
}

#[derive(Clone, Debug, PartialEq, Eq)]
enum Job {
    Number(i64),
    Operation {
        op: Op,
        monkey1: MonkeyId,
        monkey2: MonkeyId,
    },
}

fn yell(
    monkey: &MonkeyId,
    jobs: &HashMap<MonkeyId, Job>,
    cache: &mut HashMap<MonkeyId, i64>,
) -> Option<i64> {
    if let Some(cached_value) = cache.get(monkey) {
        return Some(*cached_value);
    }

    let job = jobs.get(monkey);

    if job.is_none() {
        return None;
    }

    let value: Option<i64> = match job.unwrap() {
        Job::Number(n) => Some(*n),
        Job::Operation {
            op,
            monkey1,
            monkey2,
        } => {
            let value_opt1 = yell(monkey1, jobs, cache);
            let value_opt2 = yell(monkey2, jobs, cache);
            match (value_opt1, value_opt2) {
                (Some(value1), Some(value2)) => Some(match op {
                    Op::Add => value1 + value2,
                    Op::Subtract => value1 - value2,
                    Op::Multiply => value1 * value2,
                    Op::Divide => value1 / value2,
                }),
                _ => None,
            }
        }
    };

    if let Some(n) = value {
        cache.insert(monkey.clone(), n);
    }

    value
}

fn part1(jobs: &HashMap<MonkeyId, Job>) -> i64 {
    let mut cache: HashMap<MonkeyId, i64> = HashMap::new();

    yell(&"root".into(), jobs, &mut cache).unwrap()
}

fn resolve(
    branch: &MonkeyId,
    result: i64,
    jobs: &HashMap<MonkeyId, Job>,
    cache: &HashMap<MonkeyId, i64>,
) -> i64 {
    if branch == "humn" {
        return result;
    }

    let job = jobs.get(branch).unwrap();

    match job {
        Job::Number(_) => panic!("Should not be here"),
        Job::Operation {
            op,
            monkey1,
            monkey2,
        } => {
            let cached_value1 = cache.get(monkey1);
            let cached_value2 = cache.get(monkey2);

            let (sub_branch, wanted_result): (&MonkeyId, i64) = match (cached_value1, cached_value2)
            {
                (Some(_), Some(_)) => panic!("No branches contain humn"),
                (None, None) => panic!("Both branches contain humn"),
                (None, Some(n2)) => {
                    let res: i64 = match op {
                        Op::Add => result - n2,
                        Op::Subtract => result + n2,
                        Op::Multiply => result / n2,
                        Op::Divide => result * n2,
                    };
                    (monkey1, res)
                }
                (Some(n1), None) => {
                    let res: i64 = match op {
                        Op::Add => result - n1,
                        Op::Subtract => n1 - result,
                        Op::Multiply => result / n1,
                        Op::Divide => n1 / result,
                    };
                    (monkey2, res)
                }
            };

            resolve(sub_branch, wanted_result, jobs, cache)
        }
    }
}

fn part2(mut jobs: HashMap<MonkeyId, Job>) -> i64 {
    let my_id = "humn".to_string();
    jobs.remove(&my_id);

    let (branch1, branch2) = {
        let root_job = jobs.get(&"root".to_string()).unwrap();
        if let Job::Operation {
            op: _,
            monkey1,
            monkey2,
        } = root_job
        {
            (monkey1, monkey2)
        } else {
            panic!("Root job is not an operation!");
        }
    };
    let mut cache: HashMap<MonkeyId, i64> = HashMap::new();

    let value1 = yell(branch1, &jobs, &mut cache);
    let value2 = yell(branch2, &jobs, &mut cache);

    match (value1, value2) {
        (Some(_), Some(_)) => panic!("No branches contain humn"),
        (None, None) => panic!("Both branches contain humn"),
        (None, Some(n2)) => resolve(&branch1, n2, &jobs, &cache),
        (Some(n1), None) => resolve(&branch2, n1, &jobs, &cache),
    }
}

fn main() {
    let jobs: HashMap<MonkeyId, Job> = io::stdin()
        .lines()
        .map(|l| {
            let l = l.unwrap();
            let vec: Vec<&str> = l.split(": ").collect();
            let id = vec[0].to_string();

            let vec: Vec<&str> = vec[1].split(" ").collect();

            let job = if let Ok(n) = vec[0].parse::<i64>() {
                Job::Number(n)
            } else {
                let op = match vec[1] {
                    "+" => Op::Add,
                    "-" => Op::Subtract,
                    "*" => Op::Multiply,
                    "/" => Op::Divide,
                    _ => panic!("Unknown operator"),
                };
                Job::Operation {
                    op,
                    monkey1: vec[0].to_string(),
                    monkey2: vec[2].to_string(),
                }
            };

            (id, job)
        })
        .collect();

    println!("Part 1: {}", part1(&jobs));
    println!("Part 2: {}", part2(jobs));
}
