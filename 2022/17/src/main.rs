use std::{
    collections::{BTreeMap, BTreeSet, VecDeque},
    io,
    ops::Add,
};

const WIDTH: i64 = 7;

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
enum Jet {
    Left,
    Right,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, PartialOrd, Ord)]
struct Pos {
    x: i64,
    y: i64,
}

impl Pos {
    const DELTA_SIDES: [Pos; 4] = [
        Pos { x: -1, y: 0 },
        Pos { x: 1, y: 0 },
        Pos { x: 0, y: -1 },
        Pos { x: 0, y: 1 },
    ];

    fn sides(self, x_min: i64, x_max: i64, y_min: i64, y_max: i64) -> Vec<Pos> {
        Self::DELTA_SIDES
            .iter()
            .filter_map(|d| {
                let pos = self + *d;

                if pos.x >= x_min && pos.x <= x_max && pos.y >= y_min && pos.y <= y_max {
                    Some(pos)
                } else {
                    None
                }
            })
            .collect()
    }
}

impl Add<Pos> for Pos {
    type Output = Pos;

    fn add(self, rhs: Pos) -> Self::Output {
        Pos {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

struct Shape(BTreeSet<Pos>);

impl Shape {
    fn from(positions: &[(i64, i64)]) -> Self {
        Shape(
            positions
                .into_iter()
                .map(|(x, y)| Pos { x: *x, y: *y })
                .collect(),
        )
    }
}

fn would_collide(pos: Pos, shape: &Shape, rocks: &BTreeSet<Pos>) -> bool {
    shape.0.iter().map(|p| *p + pos).any(|p| rocks.contains(&p))
}

fn debug(rocks: &BTreeSet<Pos>, falling_rock: Option<(&Shape, Pos)>, offset: Option<i64>) {
    let y_min = rocks.iter().map(|p| p.y).min().unwrap_or(0);
    let y_max = rocks.iter().map(|p| p.y).max().unwrap_or(0) + 7;
    (y_min..y_max).rev().for_each(|y| {
        print!("|");
        for x in 0..WIDTH {
            let current_pos = Pos { x, y };
            let c = if rocks.contains(&current_pos) {
                '#'
            } else if let Some(_) = falling_rock
                .and_then(|(shape, pos)| shape.0.iter().find(|p| (**p + pos) == current_pos))
            {
                '@'
            } else {
                '.'
            };
            print!("{c}");
        }
        print!("|");
        if (y + offset.unwrap_or_default()) % 5 == 0 {
            print!(" {}", y + offset.unwrap_or_default());
        }
        println!();
    });
    println!("--------- {} \n", y_min + offset.unwrap_or_default() - 1);
}

fn useful_rocks(rocks: BTreeSet<Pos>) -> BTreeSet<Pos> {
    let y_max = rocks.iter().map(|r| r.y).max().unwrap_or(0) + 1;

    let mut useful_rocks: BTreeSet<Pos> = BTreeSet::new();
    let mut greys: VecDeque<Pos> = VecDeque::new();
    let mut blacks: BTreeSet<Pos> = BTreeSet::new();

    greys.push_back(Pos { x: 0, y: y_max });

    while let Some(current_pos) = greys.pop_front() {
        for side in current_pos.sides(0, WIDTH - 1, 0, y_max) {
            if !blacks.contains(&side) {
                blacks.insert(side);
                if rocks.contains(&side) {
                    useful_rocks.insert(side);
                } else {
                    greys.push_back(side);
                }
            }
        }
    }

    useful_rocks
}

fn simulate(jets: &[Jet], shapes: &[Shape], max_rock_index: usize) -> i64 {
    let mut rocks: BTreeSet<Pos> = BTreeSet::new();
    let mut jet_index = 0;

    let mut offset: Option<i64> = None;

    // (Shape index, Jet index, offseted rocks)
    let mut memory: BTreeMap<(usize, usize, BTreeSet<Pos>), (usize, i64)> = BTreeMap::new();
    let mut rock_index = 0;
    while rock_index < max_rock_index {
        let current_shape = &shapes[rock_index % shapes.len()];
        let mut current_pos = Pos {
            x: 2,
            y: rocks.iter().map(|r| r.y).max().unwrap_or(0) + 4,
        };

        loop {
            // Pushing
            let jet = jets[jet_index];
            jet_index = (jet_index + 1) % jets.len();

            match jet {
                Jet::Left => {
                    let new_pos = current_pos + Pos { x: -1, y: 0 };
                    if new_pos.x < 0 || would_collide(new_pos, &current_shape, &rocks) {
                        // do nothing
                    } else {
                        current_pos = new_pos;
                    }
                }
                Jet::Right => {
                    let new_pos = current_pos + Pos { x: 1, y: 0 };
                    if current_shape.0.iter().any(|p| p.x + new_pos.x >= WIDTH)
                        || would_collide(new_pos, &current_shape, &rocks)
                    {
                        // do nothing
                    } else {
                        current_pos = new_pos;
                    }
                }
            }

            // Falling
            let new_pos = current_pos + (Pos { x: 0, y: -1 });
            if new_pos.y <= 0 || would_collide(new_pos, &current_shape, &rocks) {
                // The rock comes at rest
                rocks.extend(current_shape.0.iter().map(|p| *p + current_pos));

                rocks = useful_rocks(rocks);

                if offset == None {
                    let y_min = rocks.iter().map(|r| r.y).min().unwrap_or(0);
                    let offsetted_rocks = rocks
                        .iter()
                        .map(|r| Pos {
                            x: r.x,
                            y: r.y - y_min,
                        })
                        .collect();

                    let memory_piece = (rock_index % shapes.len(), jet_index, offsetted_rocks);
                    let height = rocks.iter().map(|p| p.y).max().unwrap();
                    if let Some((last_rock_index, last_height)) = memory.get(&memory_piece) {
                        let diff_rock_index = rock_index - last_rock_index;
                        let diff_height = height - last_height;

                        let nb_remaining_occurences =
                            (max_rock_index - rock_index - 1) / diff_rock_index;

                        rock_index += nb_remaining_occurences * diff_rock_index;
                        offset = Some(nb_remaining_occurences as i64 * diff_height);
                    } else {
                        memory.insert(memory_piece, (rock_index, height));
                    }
                }
                break;
            } else {
                current_pos = new_pos;
            }
        }
        rock_index += 1;
    }

    debug(&rocks, None, offset);

    rocks.iter().map(|p| p.y).max().unwrap() + offset.unwrap_or(0)
}

fn main() {
    let shapes: [Shape; 5] = [
        Shape::from(&[(0, 0), (1, 0), (2, 0), (3, 0)]),
        Shape::from(&[(1, 0), (0, 1), (1, 1), (2, 1), (1, 2)]),
        Shape::from(&[(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)]), // y towards top
        Shape::from(&[(0, 0), (0, 1), (0, 2), (0, 3)]),
        Shape::from(&[(0, 0), (0, 1), (1, 0), (1, 1)]),
    ];
    let jets: Vec<Jet> = io::stdin()
        .lines()
        .next()
        .unwrap()
        .unwrap()
        .chars()
        .map(|c| match c {
            '<' => Jet::Left,
            '>' => Jet::Right,
            _ => panic!("unknown char"),
        })
        .collect();

    println!("Part 1: {}", simulate(&jets, &shapes, 2022));
    println!("Part 2: {}", simulate(&jets, &shapes, 1_000_000_000_000));
}
