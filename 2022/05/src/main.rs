use std::{io, iter::repeat};

use nom::{bytes::complete::tag, character::complete::u32, IResult};

fn read_stacks(input: &[String]) -> Vec<Vec<char>> {
    let mut raw_crates = input
        .iter()
        .map(|l| {
            l.chars()
                .enumerate()
                .filter_map(|(i, c)| if i % 4 == 1 { Some(c) } else { None })
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    let stack_count = raw_crates.last().unwrap().len();
    // Remove the last line which is the stack numbers.
    raw_crates.pop();
    // Make it immutable
    let raw_crates = raw_crates;

    let mut stacks: Vec<Vec<char>> = repeat(Vec::with_capacity(60))
        .take(stack_count)
        .collect::<Vec<_>>();

    for line in raw_crates.iter().rev() {
        for i in 0..stack_count {
            if let Some(&c) = line.get(i) {
                if c != ' ' {
                    stacks[i].push(c);
                }
            }
        }
    }

    stacks
}

#[derive(Debug)]
struct Instruction {
    count: usize,
    from: usize,
    to: usize,
}

fn read_instructions(lines: &[String]) -> Vec<Instruction> {
    lines
        .iter()
        .map(|input| -> IResult<&str, Instruction> {
            let (input, _) = tag("move ")(input as &str)?;
            let (input, count) = u32(input)?;
            let (input, _) = tag(" from ")(input)?;
            let (input, from) = u32(input)?;
            let (input, _) = tag(" to ")(input)?;
            let (input, to) = u32(input)?;

            Ok((
                input,
                Instruction {
                    count: count as usize,
                    from: from as usize,
                    to: to as usize,
                },
            ))
        })
        .map(|r| r.unwrap().1)
        .collect::<Vec<_>>()
}

fn part1(mut stacks: Vec<Vec<char>>, instructions: &[Instruction]) -> String {
    for instr in instructions {
        for _ in 0..instr.count {
            let elem = stacks[instr.from - 1].pop().unwrap();
            stacks[instr.to - 1].push(elem);
        }
    }

    stacks.iter().filter_map(|s| s.last()).collect::<String>()
}

fn part2(mut stacks: Vec<Vec<char>>, instructions: &[Instruction]) -> String {
    for instr in instructions {
        let range = (stacks[instr.from - 1].len() - instr.count)..;

        let elems = stacks[instr.from - 1].splice(range, []).collect::<Vec<_>>();

        for e in elems {
            stacks[instr.to - 1].push(e);
        }
    }

    stacks.iter().filter_map(|s| s.last()).collect::<String>()
}

fn main() {
    let lines = io::stdin().lines().collect::<Result<Vec<_>, _>>().unwrap();
    let stack_lines = lines
        .iter()
        .take_while(|l| !l.is_empty())
        .cloned()
        .collect::<Vec<_>>();
    let instruction_lines = lines
        .iter()
        .skip(stack_lines.len() + 1)
        .cloned()
        .collect::<Vec<_>>();

    let stacks = read_stacks(&stack_lines);
    let instructions = read_instructions(&instruction_lines);

    println!("Part 1: {}", part1(stacks.clone(), &instructions));
    println!("Part 2: {}", part2(stacks, &instructions));
}
