use std::{
    cell::RefCell,
    io,
    rc::{Rc, Weak},
};

use nom::{
    branch::alt,
    bytes::streaming::tag,
    character::complete::{self, alpha1, newline, not_line_ending},
    combinator::complete,
    multi::{separated_list0, separated_list1},
    sequence::{pair, separated_pair},
    IResult,
};

// **************************************************************************************************
// Parsing
// **************************************************************************************************

#[derive(PartialEq, Eq, Debug, Clone)]
enum Object {
    File { name: String, size: usize },
    Folder { name: String },
}

#[derive(PartialEq, Eq, Debug)]
enum Command {
    List(Vec<Object>),
    MoveUp,
    MoveDown(String),
    MoveToRoot,
}

fn cd(input: &str) -> IResult<&str, Command> {
    let (input, (_, arg)) = pair(tag("$ cd "), alt((alpha1, tag("/"), tag(".."))))(input)?;

    let cmd = match arg {
        "/" => Command::MoveToRoot,
        ".." => Command::MoveUp,
        dir => Command::MoveDown(dir.to_string()),
    };

    Ok((input, cmd))
}

fn file(input: &str) -> IResult<&str, Object> {
    let (input, (size, name)) = separated_pair(complete::u32, tag(" "), not_line_ending)(input)?;

    Ok((
        input,
        Object::File {
            name: name.to_string(),
            size: size as usize,
        },
    ))
}

fn dir(input: &str) -> IResult<&str, Object> {
    let (input, (_, name)) = pair(tag("dir "), alpha1)(input)?;

    Ok((
        input,
        Object::Folder {
            name: name.to_string(),
        },
    ))
}

fn ls(input: &str) -> IResult<&str, Command> {
    let (input, (_, sub_objects)) = separated_pair(
        tag("$ ls"),
        newline,
        separated_list0(newline, alt((file, dir))),
    )(input)?;

    Ok((input, Command::List(sub_objects)))
}

fn read_commands() -> Vec<Command> {
    let input = io::stdin()
        .lines()
        .collect::<Result<Vec<String>, _>>()
        .unwrap()
        .join("\n");

    let (_, cmds) = complete(separated_list1(newline, alt((cd, ls))))(&input).unwrap();

    cmds
}

// **************************************************************************************************
// Building tree
// **************************************************************************************************

#[derive(Debug)]
struct TreeNode {
    object: Object,
    children: Vec<TreeNode>,
}

impl TreeNode {
    fn _print(&self, depth: usize) {
        let tab = std::iter::repeat("  ").take(depth).collect::<String>();
        match &self.object {
            Object::File { name, size } => println!("{tab}{name} ({size})"),
            Object::Folder { name } => {
                println!("{tab}{name}");
                self.children.iter().for_each(|n| n._print(depth + 1));
            }
        }
    }
}

struct BuilderTreeNode {
    object: Object,
    children: Vec<Rc<RefCell<BuilderTreeNode>>>,
    parent: Option<Weak<RefCell<BuilderTreeNode>>>,
}

impl BuilderTreeNode {
    fn new(object: &Object, parent: Option<Rc<RefCell<BuilderTreeNode>>>) -> BuilderTreeNode {
        BuilderTreeNode {
            object: object.clone(),
            children: Vec::new(),
            parent: parent.map(|r| Rc::downgrade(&r)),
        }
    }

    fn build(this: &Rc<RefCell<BuilderTreeNode>>) -> TreeNode {
        let this = this.borrow();
        TreeNode {
            object: this.object.clone(),
            children: this
                .children
                .iter()
                .map(|n| Self::build(n))
                .collect::<Vec<_>>(),
        }
    }
}

fn build_tree(cmds: &[Command]) -> TreeNode {
    let root = Rc::new(RefCell::new(BuilderTreeNode::new(
        &Object::Folder {
            name: "/".to_string(),
        },
        None,
    )));

    let mut current = root.clone();

    for cmd in cmds {
        match cmd {
            Command::MoveToRoot => {
                current = root.clone();
            }
            Command::MoveUp => {
                if let Some(dir) = &current.clone().borrow().parent {
                    current = dir.upgrade().unwrap().clone();
                };
            }
            Command::MoveDown(dir) => {
                if let Some(node) = current.clone().borrow().children.iter().find(|n| {
                    if let Object::Folder { name } = &n.borrow().object {
                        if name == dir {
                            return true;
                        }
                    }
                    false
                }) {
                    current = node.clone();
                } else {
                    panic!("Can't cd to {}", dir);
                };
            }
            Command::List(children) => {
                if !children.is_empty() && current.borrow().children.is_empty() {
                    for c in children {
                        let node =
                            Rc::new(RefCell::new(BuilderTreeNode::new(c, Some(current.clone()))));
                        current.borrow_mut().children.push(node);
                    }
                }
            }
        }
    }

    BuilderTreeNode::build(&root)
}

// **************************************************************************************************
// Actually answering the questions
// **************************************************************************************************

fn solve_problems(tree: &TreeNode) {
    let mut folder_sizes: Vec<usize> = Vec::new();

    fn iter_fn(n: &TreeNode, folder_sizes: &mut Vec<usize>) -> usize {
        match &n.object {
            Object::File { name: _, size } => *size, // do nothing
            Object::Folder { name: _ } => {
                let size = n.children.iter().map(|n| iter_fn(&n, folder_sizes)).sum();
                folder_sizes.push(size);

                size
            }
        }
    }

    iter_fn(tree, &mut folder_sizes);

    let part1: usize = folder_sizes.iter().filter(|s| **s <= 100_000).sum();

    folder_sizes.sort();

    let free_space = 70_000_000 - folder_sizes.last().unwrap();
    let min_size_to_free = 30_000_000 - free_space;

    let part2 = folder_sizes
        .iter()
        .find(|&&s| s >= min_size_to_free)
        .unwrap();

    dbg!(&folder_sizes);
    dbg!(min_size_to_free);
    println!("Part 1: {part1}");
    println!("Part 2: {part2}");
}

fn main() {
    let cmds = read_commands();
    let tree = build_tree(&cmds);

    // tree.print(0);
    solve_problems(&tree);
}
