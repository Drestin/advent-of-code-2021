use std::{collections::BTreeSet, io};

type Pos = (usize, usize);

fn part1(grid: &Vec<Vec<usize>>) -> usize {
    let width = grid[0].len();
    let height = grid.len();

    // Doesn't contain trees at the edges
    let mut visible_trees: BTreeSet<Pos> = BTreeSet::new();

    // O(N) with N the number of trees
    for x in 1..width - 1 {
        // Visible from top
        let mut last_visible_tree_from_top = 0;
        let mut highest_tree = grid[0][x];
        for y in 1..height - 1 {
            if grid[y][x] > highest_tree {
                visible_trees.insert((x, y));
                highest_tree = grid[y][x];
                last_visible_tree_from_top = y;

                if highest_tree == 9 {
                    break;
                }
            }
        }
        // Visible from bottom
        let mut highest_tree = grid[height - 1][x];
        for y in (last_visible_tree_from_top + 1..=height - 1).rev() {
            if grid[y][x] > highest_tree {
                visible_trees.insert((x, y));
                highest_tree = grid[y][x];

                if highest_tree == 9 {
                    break;
                }
            }
        }
    }

    for y in 1..height - 1 {
        // Visible from left
        let mut last_visible_tree_from_left = 0;
        let mut highest_tree = grid[y][0];
        for x in 1..width - 1 {
            if grid[y][x] > highest_tree {
                visible_trees.insert((x, y));
                highest_tree = grid[y][x];
                last_visible_tree_from_left = x;

                if highest_tree == 9 {
                    break;
                }
            }
        }
        // Visible from right
        let mut highest_tree = grid[y][height - 1];
        for x in (last_visible_tree_from_left + 1..=width - 2).rev() {
            if grid[y][x] > highest_tree {
                visible_trees.insert((x, y));
                highest_tree = grid[y][x];

                if highest_tree == 9 {
                    break;
                }
            }
        }
    }

    // Count the visible trees and add the edge trees
    visible_trees.len() + 2 * height + 2 * width - 4
}

fn calculate_scenic_score(grid: &Vec<Vec<usize>>, house_x: usize, house_y: usize) -> usize {
    let width = grid[0].len();
    let height = grid.len();

    let house_height = grid[house_y][house_x];

    let mut left_visible_trees = 0;
    for x in (0..house_x).rev() {
        left_visible_trees += 1;
        if house_height <= grid[house_y][x] {
            break;
        }
    }

    let mut right_visible_trees = 0;
    for x in house_x + 1..height {
        right_visible_trees += 1;
        if house_height <= grid[house_y][x] {
            break;
        }
    }

    let mut top_visible_trees = 0;
    for y in (0..house_y).rev() {
        top_visible_trees += 1;
        if house_height <= grid[y][house_x] {
            break;
        }
    }

    let mut bottom_visible_trees = 0;
    for y in house_y + 1..width {
        bottom_visible_trees += 1;
        if house_height <= grid[y][house_x] {
            break;
        }
    }

    left_visible_trees * right_visible_trees * top_visible_trees * bottom_visible_trees
}

fn part2(grid: &Vec<Vec<usize>>) -> usize {
    let width = grid[0].len();
    let height = grid.len();
    let mut scenic_score_max = 0;

    for house_x in 0..width {
        for house_y in 0..height {
            let scenic_score = calculate_scenic_score(grid, house_x, house_y);
            if scenic_score > scenic_score_max {
                scenic_score_max = scenic_score;
            }
        }
    }

    scenic_score_max
}

fn main() {
    let grid = io::stdin()
        .lines()
        .map(|l| {
            l.unwrap()
                .chars()
                .map(|c| c.to_digit(10).unwrap() as usize)
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    println!("Part 1: {}", part1(&grid));
    println!("Part 2: {}", part2(&grid));
}
