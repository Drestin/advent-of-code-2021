use std::io;

use nom::{bytes::complete::tag, character::complete::u64, IResult};
use priority_queue::PriorityQueue;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Resource {
    Ore = 0,
    Clay,
    Obsidian,
    Geode,
}

impl Resource {
    const COUNT: usize = 4;

    const ALL: [Resource; Resource::COUNT] = [
        Resource::Ore,
        Resource::Clay,
        Resource::Obsidian,
        Resource::Geode,
    ];
}

#[derive(Debug)]
struct Blueprint {
    id: u64,
    costs: [[u64; Resource::COUNT]; Resource::COUNT],
}

fn parse_blueprint(input: &str) -> IResult<&str, Blueprint> {
    let (input, _) = tag("Blueprint ")(input)?;
    let (input, id) = u64(input)?;
    let (input, _) = tag(": Each ore robot costs ")(input)?;
    let (input, ore_cost) = u64(input)?;
    let (input, _) = tag(" ore. Each clay robot costs ")(input)?;
    let (input, clay_cost) = u64(input)?;
    let (input, _) = tag(" ore. Each obsidian robot costs ")(input)?;
    let (input, obsi_cost_in_ore) = u64(input)?;
    let (input, _) = tag(" ore and ")(input)?;
    let (input, obsi_cost_in_clay) = u64(input)?;
    let (input, _) = tag(" clay. Each geode robot costs ")(input)?;
    let (input, geode_cost_in_ore) = u64(input)?;
    let (input, _) = tag(" ore and ")(input)?;
    let (input, geode_cost_in_obsidian) = u64(input)?;
    let (input, _) = tag(" obsidian.")(input)?;

    let blueprint = Blueprint {
        id,
        costs: [
            [ore_cost, 0, 0, 0],
            [clay_cost, 0, 0, 0],
            [obsi_cost_in_ore, obsi_cost_in_clay, 0, 0],
            [geode_cost_in_ore, 0, geode_cost_in_obsidian, 0],
        ],
    };

    Ok((input, blueprint))
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct State {
    time: u64,
    robots: [u64; Resource::COUNT],
    resources: [u64; Resource::COUNT],
}

fn triangular_sum(n: u64) -> u64 {
    if n <= 1 {
        0
    } else {
        (n * (n + 1)) / 2
    }
}

impl State {
    fn possible_next_states(mut self, blueprint: &Blueprint, max_time: u64) -> Vec<State> {
        let next_states: Vec<State> = Resource::ALL
            .iter()
            .filter_map(|&robot| {
                if !self.will_be_able_to_produce_robot(blueprint, robot) {
                    return None;
                }

                let mut new_state = self.clone();
                new_state.go_to_robot_production(blueprint, robot);

                if new_state.time <= max_time {
                    Some(new_state)
                } else {
                    None
                }
            })
            .collect();

        if next_states.is_empty() {
            while self.time < max_time {
                self.advance_time(blueprint, None, max_time - self.time);
            }
            vec![self]
        } else {
            next_states
        }
    }

    fn go_to_robot_production(&mut self, blueprint: &Blueprint, robot: Resource) {
        while !self.can_produce_robot(blueprint, robot) {
            self.advance_time(blueprint, None, 1);
        }

        self.advance_time(blueprint, Some(robot), 1);
    }

    fn advance_time(
        &mut self,
        blueprint: &Blueprint,
        robot_production_opt: Option<Resource>,
        delta: u64,
    ) {
        self.time += delta;
        for i in 0..Resource::COUNT {
            self.resources[i] += self.robots[i] * delta;
        }
        if let Some(robot) = robot_production_opt {
            self.robots[robot as usize] += 1;

            for i in 0..Resource::COUNT {
                self.resources[i] -= blueprint.costs[robot as usize][i];
            }
        }
    }

    fn can_produce_robot(&self, blueprint: &Blueprint, robot: Resource) -> bool {
        let robot_cost = blueprint.costs[robot as usize];

        (0..Resource::COUNT).all(|i| self.resources[i] >= robot_cost[i])
    }

    /// If it will be able to produce a robot of the given type, if no
    /// other robots are produced before.
    fn will_be_able_to_produce_robot(&self, blueprint: &Blueprint, robot: Resource) -> bool {
        let robot_cost = blueprint.costs[robot as usize];
        (0..Resource::COUNT).all(|res| robot_cost[res] == 0 || self.robots[res] > 0)
    }

    fn geode_count(&self) -> u64 {
        self.resources[Resource::Geode as usize]
    }

    fn priority(&self) -> u64 {
        let mut factor: u64 = 1;
        let mut total: u64 = 0;
        for r in 0..Resource::COUNT {
            factor *= 100;
            total += factor * self.robots[r];
        }

        total / (self.time + 1)
    }

    fn helper_max_resource(&self, max_time: u64, resource: Resource) -> u64 {
        let remaining_time = max_time - self.time;
        let already_produced = self.resources[resource as usize];
        let already_producing = remaining_time * self.robots[resource as usize];

        let possible_if_we_create_robot_each_turn = triangular_sum(remaining_time);

        already_produced + already_producing + possible_if_we_create_robot_each_turn
    }

    fn max_possible(&self, blueprint: &Blueprint, max_time: u64) -> u64 {
        let max_obsidian = self.helper_max_resource(max_time, Resource::Obsidian);
        let max_built_robot =
            max_obsidian / blueprint.costs[Resource::Geode as usize][Resource::Obsidian as usize];

        self.helper_max_resource(max_time, Resource::Geode)
            - triangular_sum(max_time - self.time - max_built_robot + 1)
    }
}

fn max_geode_produced(blueprint: &Blueprint, max_time: u64) -> u64 {
    // println!("###### Blueprint {}:", blueprint.id);
    let start_state: State = State {
        resources: [0; 4],
        robots: [1, 0, 0, 0],
        time: 0,
    };
    let mut states: PriorityQueue<State, u64> = PriorityQueue::new();
    let prio = start_state.priority();
    states.push(start_state, prio);

    let mut cut_by_max: usize = 0;
    let mut current_max: u64 = 0;

    let mut i = 0;
    while let Some((current_state, _)) = states.pop() {
        if current_state.time < max_time {
            for next_state in current_state.possible_next_states(blueprint, max_time) {
                if next_state.max_possible(blueprint, max_time) > current_max {
                    let prio = next_state.priority();
                    states.push(next_state, prio);
                } else {
                    cut_by_max += 1;
                }
            }
        } else {
            current_max = current_max.max(current_state.geode_count());
        }
        i += 1;
        if i % 10_000_000 == 0 {
            println!("seen: {i} ; cut by max: {cut_by_max} ; current max: {current_max}");
        }
    }

    println!(
        "Blueprint {} finished ; Seen {} ; Cut: {} ; Max: {}",
        blueprint.id, i, cut_by_max, current_max
    );
    current_max
}

fn part1(blueprints: &[Blueprint]) -> u64 {
    blueprints
        .iter()
        .map(|bp| max_geode_produced(bp, 24) * bp.id)
        .sum()
}

fn part2(blueprints: &[Blueprint]) -> u64 {
    blueprints
        .iter()
        .take(3)
        .map(|bp| max_geode_produced(bp, 32))
        .product()
}

fn main() {
    let blueprints: Vec<Blueprint> = io::stdin()
        .lines()
        .map(|l| parse_blueprint(&l.unwrap()).unwrap().1)
        .collect();

    println!("Part 1: {}", part1(&blueprints));
    println!("Part 2: {}", part2(&blueprints));
}
