use std::io;

fn part1(input: &[String]) -> usize {
    input
        .split(|s| s.is_empty())
        .map(|a| a.iter().map(|s| s.parse::<usize>().unwrap()).sum())
        .max()
        .unwrap()
}

fn part2(input: &[String]) -> usize {
    let mut vec = input
        .split(|s| s.is_empty())
        .map(|a| a.iter().map(|s| s.parse::<usize>().unwrap()).sum())
        .collect::<Vec<usize>>();

    vec.sort_by(|a, b| b.cmp(a));

    vec.iter().take(3).sum()
}

fn main() {
    let input = io::stdin()
        .lines()
        .collect::<Result<Vec<String>, _>>()
        .unwrap();

    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
}
