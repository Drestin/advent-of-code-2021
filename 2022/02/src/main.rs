use std::io;

#[derive(PartialEq, Eq, Clone, Copy)]
enum Shape {
    Rock,
    Paper,
    Scissors,
}

impl Shape {
    fn score(&self) -> usize {
        match self {
            Shape::Rock => 1,
            Shape::Paper => 2,
            Shape::Scissors => 3,
        }
    }

    fn from_char(c: &char) -> Self {
        match c {
            'A' | 'X' => Shape::Rock,
            'B' | 'Y' => Shape::Paper,
            'C' | 'Z' => Shape::Scissors,
            _ => panic!("unknown shape: {c}"),
        }
    }

    fn from_opponent_and_wanted_result(op: &Self, result: &Result) -> Self {
        match (result, op) {
            (Result::Tie, _) => op.clone(),
            (Result::Lose, Shape::Rock) => Shape::Scissors,
            (Result::Lose, Shape::Paper) => Shape::Rock,
            (Result::Lose, Shape::Scissors) => Shape::Paper,
            (Result::Win, Shape::Rock) => Shape::Paper,
            (Result::Win, Shape::Paper) => Shape::Scissors,
            (Result::Win, Shape::Scissors) => Shape::Rock,
        }
    }
}

#[derive(Clone, Copy)]
enum Result {
    Win,
    Tie,
    Lose,
}

impl Result {
    fn score(&self) -> usize {
        match self {
            Result::Lose => 0,
            Result::Tie => 3,
            Result::Win => 6,
        }
    }

    fn from_char(c: &char) -> Self {
        match c {
            'X' => Result::Lose,
            'Y' => Result::Tie,
            'Z' => Result::Win,
            _ => panic!("unknown input: {c}"),
        }
    }
}

struct Round {
    me: Shape,
    op: Shape,
}

impl Round {
    fn result(&self) -> Result {
        match (self.me, self.op) {
            _ if (self.me == self.op) => Result::Tie,
            (Shape::Rock, Shape::Scissors) => Result::Win,
            (Shape::Scissors, Shape::Paper) => Result::Win,
            (Shape::Paper, Shape::Rock) => Result::Win,
            _ => Result::Lose,
        }
    }

    fn score(&self) -> usize {
        self.me.score() + self.result().score()
    }
}

fn part1(input: &[(char, char)]) -> usize {
    input
        .iter()
        .map(|(op_char, me_char)| {
            Round {
                me: Shape::from_char(me_char),
                op: Shape::from_char(op_char),
            }
            .score()
        })
        .sum()
}

fn part2(input: &[(char, char)]) -> usize {
    input
        .iter()
        .map(|(op_char, res_char)| {
            let op = Shape::from_char(op_char);
            let res = Result::from_char(res_char);

            Round {
                op,
                me: Shape::from_opponent_and_wanted_result(&op, &res),
            }
            .score()
        })
        .sum()
}

fn main() {
    let input: Vec<(char, char)> = io::stdin()
        .lines()
        .map(|line| {
            if let [c0, ' ', c1] = line.unwrap().chars().collect::<Vec<char>>().as_slice() {
                (*c0, *c1)
            } else {
                panic!("Failed to parse");
            }
        })
        .collect();

    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
}
