use std::{
    collections::HashSet,
    io,
    ops::{Add, AddAssign},
};

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
struct Pos {
    x: i32,
    y: i32,
}

impl Add<&Pos> for &Pos {
    type Output = Pos;

    fn add(self, rhs: &Pos) -> Self::Output {
        Pos {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl AddAssign<&Pos> for Pos {
    fn add_assign(&mut self, rhs: &Pos) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

struct StepIterator {
    current: Pos,
    step: Pos,
    end: Pos,
    started: bool,
}

impl Iterator for StepIterator {
    type Item = Pos;

    fn next(&mut self) -> Option<Self::Item> {
        if !self.started {
            self.started = true;
            Some(self.current.clone())
        } else if self.current == self.end {
            None
        } else {
            self.current += &self.step;
            Some(self.current.clone())
        }
    }
}

impl Pos {
    fn delta(&self, end: &Pos) -> Self {
        Pos {
            x: (end.x - self.x).signum(),
            y: (end.y - self.y).signum(),
        }
    }

    fn steps(&self, end: &Pos) -> impl Iterator<Item = Self> {
        StepIterator {
            current: self.clone(),
            step: self.delta(end),
            end: end.clone(),
            started: false,
        }
    }
}

const SAND_START: Pos = Pos { x: 500, y: 0 };

const FALLING_DELTAS: [Pos; 3] = [Pos { x: 0, y: 1 }, Pos { x: -1, y: 1 }, Pos { x: 1, y: 1 }];

#[derive(Debug, Clone)]
struct Caves {
    rocks: HashSet<Pos>,
    bottom: i32,
    sand: HashSet<Pos>,
    have_floor: bool,
}

impl Caves {
    fn from_lines(lines: impl Iterator<Item = String>) -> Self {
        let mut caves = Self {
            bottom: 0,
            rocks: HashSet::new(),
            sand: HashSet::new(),
            have_floor: false,
        };

        for line in lines {
            let points = line
                .split(" -> ")
                .map(|w| {
                    let mut iter = w.split(",").map(|n| n.parse::<i32>().unwrap());
                    Pos {
                        x: iter.next().unwrap(),
                        y: iter.next().unwrap(),
                    }
                })
                .collect::<Vec<Pos>>();
            let mut point_pairs_iter = points.windows(2);

            while let Some([a, b]) = point_pairs_iter.next() {
                for pos in a.steps(b) {
                    if pos.y > caves.bottom {
                        caves.bottom = pos.y;
                    }
                    caves.rocks.insert(pos);
                }
            }
        }

        caves
    }

    fn add_floor(&mut self) {
        self.bottom += 1;
        self.have_floor = true;
    }

    // Return true if the sand came to a stop
    fn add_sand(&mut self) -> bool {
        let mut sand_pos = SAND_START.clone();

        'outer: while sand_pos.y < self.bottom {
            for delta in FALLING_DELTAS {
                let new_pos = &sand_pos + &delta;

                if !self.rocks.contains(&new_pos) && !self.sand.contains(&new_pos) {
                    sand_pos = new_pos;
                    continue 'outer;
                }
            }

            // The sand stopped
            self.sand.insert(sand_pos);
            return true;
        }

        if self.have_floor {
            self.sand.insert(sand_pos);
            true
        } else {
            // The sand didn't stop and falled into the void
            false
        }
    }

    fn print(&self) {
        let x_min = self.rocks.iter().map(|p| p.x).min().unwrap() - 1;
        let x_max = self.rocks.iter().map(|p| p.x).max().unwrap() + 1;
        let y_min = 0;
        let y_max = self.bottom + 1;

        for y in y_min..=y_max {
            for x in x_min..=x_max {
                let pos = Pos { x, y };
                let c = if self.rocks.contains(&pos) {
                    '#'
                } else if self.sand.contains(&pos) {
                    'o'
                } else {
                    '.'
                };
                print!("{c}");
            }
            println!();
        }
        println!();
    }
}

fn part1(mut caves: Caves) -> usize {
    loop {
        let sand_stopped = caves.add_sand();
        if !sand_stopped {
            return caves.sand.len();
        }
    }
}

fn part2(mut caves: Caves) -> usize {
    caves.add_floor();
    loop {
        let sand_stopped = caves.add_sand();
        if !sand_stopped {
            panic!("The sand falled into the void!")
        }

        if caves.sand.contains(&SAND_START) {
            return caves.sand.len();
        }
    }
}

fn main() {
    let caves = Caves::from_lines(io::stdin().lines().map(|l| l.unwrap()));
    caves.print();
    println!("Part 1: {}", part1(caves.clone()));
    println!("Part 2: {}", part2(caves));
}
