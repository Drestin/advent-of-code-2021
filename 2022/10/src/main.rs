use std::io;

enum Instruction {
    NoOp,
    AddX(isize),
}

struct RegisterIterator<'a> {
    instructions: Box<dyn Iterator<Item = &'a Instruction> + 'a>,
    register_x: isize,
    next_register_x: isize,
    second_instruction: bool,
}

impl RegisterIterator<'_> {
    fn new<'a, I>(instructions: I) -> RegisterIterator<'a>
    where
        I: IntoIterator<Item = &'a Instruction>,
        I::IntoIter: 'a,
    {
        RegisterIterator {
            instructions: Box::new(instructions.into_iter()),
            register_x: 1,
            next_register_x: 1,
            second_instruction: false,
        }
    }
}

impl Iterator for RegisterIterator<'_> {
    type Item = isize;

    fn next(&mut self) -> Option<Self::Item> {
        if self.second_instruction {
            self.second_instruction = false;
            Some(self.register_x)
        } else if let Some(instr) = self.instructions.next() {
            self.register_x = self.next_register_x;

            if let Instruction::AddX(add_x) = instr {
                self.next_register_x += add_x;
                self.second_instruction = true;
            }

            Some(self.register_x)
        } else {
            None
        }
    }
}

/// Same semantic as iter_register but using a hand-made iterator
fn iter_register2<'a>(input: &'a [Instruction]) -> impl Iterator<Item = isize> + 'a {
    RegisterIterator::new(input)
}

fn iter_register<'a>(input: &'a [Instruction]) -> impl Iterator<Item = isize> + 'a {
    let start = 1 as isize;
    let iterator = input
        .iter()
        .scan(start, |reg, instr| match instr {
            Instruction::NoOp => Some(vec![*reg]),
            Instruction::AddX(n) => {
                let old_reg = *reg;
                *reg += n;
                Some(vec![old_reg, *reg])
            }
        })
        .flatten();

    vec![start].into_iter().chain(iterator)
}

fn part1(input: &[Instruction]) -> isize {
    iter_register(input)
        .enumerate()
        .filter_map(|(i, x)| {
            let i = i + 1;
            if i <= 220 && i % 40 == 20 {
                Some(i as isize * x)
            } else {
                None
            }
        })
        .sum()
}

fn part2(input: &[Instruction]) {
    for (i, reg_x) in iter_register2(input).enumerate() {
        let x = ((i % 40) + 1) as isize;
        if x == 1 {
            println!();
        }

        let c = if reg_x <= x && reg_x + 2 >= x {
            '#'
        } else {
            '.'
        };

        print!("{c}");
    }
}

fn main() {
    let input = io::stdin()
        .lines()
        .map(|l| {
            let l = l.unwrap();
            let cmd = &l[..4];
            match cmd {
                "noop" => Instruction::NoOp,
                "addx" => {
                    let arg = &l[5..];
                    Instruction::AddX(arg.parse().unwrap())
                }
                _ => panic!("Failed to parse {}", l),
            }
        })
        .collect::<Vec<_>>();

    println!("Part 1: {}", part1(&input));
    println!("Part 2:");
    part2(&input);
}
