use std::{
    collections::BTreeSet,
    io,
    iter::repeat,
    ops::{Add, AddAssign},
};

#[derive(PartialEq, Eq, Debug)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl Direction {
    fn delta(&self) -> Pos {
        match self {
            Direction::Left => Pos { x: -1, y: 0 },
            Direction::Right => Pos { x: 1, y: 0 },
            Direction::Up => Pos { x: 0, y: -1 },
            Direction::Down => Pos { x: 0, y: 1 },
        }
    }
}

struct Move {
    dir: Direction,
    n: usize,
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
struct Pos {
    x: isize,
    y: isize,
}

impl Pos {
    const ZERO: Pos = Pos { x: 0, y: 0 };
}

impl Add<Pos> for Pos {
    type Output = Pos;

    fn add(self, rhs: Pos) -> Self::Output {
        Pos {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl AddAssign<Pos> for Pos {
    fn add_assign(&mut self, rhs: Pos) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

fn move_tail(tail: &mut Pos, head: Pos) {
    if tail.x - head.x == 0 {
        // Horizontal
        if head.y <= tail.y - 2 {
            tail.y -= 1;
        } else if head.y >= tail.y + 2 {
            tail.y += 1;
        }
    } else if tail.y - head.y == 0 {
        // Vertical
        if head.x <= tail.x - 2 {
            tail.x -= 1;
        } else if head.x >= tail.x + 2 {
            tail.x += 1;
        }
    } else if (head.x - tail.x).abs() + (head.y - tail.y).abs() >= 3 {
        // Diagonal
        tail.x += (head.x - tail.x).signum();
        tail.y += (head.y - tail.y).signum();
    }
}

fn simulate_rope(moves: &[Move], rope_length: usize) -> usize {
    let mut visited_positions: BTreeSet<Pos> = BTreeSet::new();
    let mut positions: Vec<Pos> = repeat(Pos::ZERO).take(rope_length).collect::<Vec<_>>();

    visited_positions.insert(Pos::ZERO);

    for mov in moves {
        for _step in 0..mov.n {
            positions[0] += mov.dir.delta();
            for i in 1..rope_length {
                let head = positions[i - 1];
                move_tail(&mut positions[i], head);
            }

            visited_positions.insert(positions[rope_length - 1]);
        }
    }

    visited_positions.len()
}

fn part1(moves: &[Move]) -> usize {
    simulate_rope(moves, 2)
}

fn part2(moves: &[Move]) -> usize {
    simulate_rope(moves, 10)
}

fn main() {
    let moves = io::stdin()
        .lines()
        .map(|l| {
            let l = l.unwrap();
            let words = l.split(" ").collect::<Vec<_>>();

            let dir = match words[0] {
                "L" => Direction::Left,
                "R" => Direction::Right,
                "U" => Direction::Up,
                "D" => Direction::Down,
                _ => panic!("Failed to parse direction"),
            };

            let n = words[1].parse::<usize>().unwrap();

            Move { dir, n }
        })
        .collect::<Vec<_>>();

    println!("Part 1: {}", part1(&moves));
    println!("Part 2: {}", part2(&moves));
}
