use std::{collections::VecDeque, io};

#[derive(Debug, Clone, Copy)]
enum Operation {
    Add(usize),
    Mult(usize),
    Square,
}

impl Operation {
    fn apply(&self, item: usize) -> usize {
        match self {
            Operation::Add(n) => item + n,
            Operation::Mult(n) => item * n,
            Operation::Square => item * item,
        }
    }
}

#[derive(Debug, Clone)]
struct Monkey {
    items: VecDeque<usize>,
    op: Operation,
    divide_test: usize,
    on_true: usize,
    on_false: usize,
    inspection_count: usize,
}

fn parse_monkeys(lines: &[String]) -> Vec<Monkey> {
    lines
        .chunks(7)
        .map(|monkey_lines| {
            let items = monkey_lines[1]["  Starting items: ".len()..]
                .split(", ")
                .map(|s| s.parse::<usize>().unwrap())
                .collect::<VecDeque<usize>>();
            let divide_test: usize = monkey_lines[3]["  Test: divisible by ".len()..]
                .parse()
                .unwrap();
            let on_true: usize = monkey_lines[4]["    If true: throw to monkey ".len()..]
                .parse()
                .unwrap();
            let on_false: usize = monkey_lines[5]["    If false: throw to monkey ".len()..]
                .parse()
                .unwrap();
            let op_str = &monkey_lines[2]["  Operation: new = old ".len()..];
            let op = if op_str == "* old" {
                Operation::Square
            } else {
                let n: usize = op_str[2..].parse().unwrap();
                if &op_str[0..1] == "+" {
                    Operation::Add(n)
                } else {
                    Operation::Mult(n)
                }
            };

            Monkey {
                items,
                op,
                divide_test,
                on_true,
                on_false,
                inspection_count: 0,
            }
        })
        .collect::<Vec<_>>()
}

fn play_round(monkeys: &mut [Monkey], divide_by_three: bool) {
    let global_modulus: usize = monkeys.iter().map(|m| m.divide_test).product();

    for i in 0..monkeys.len() {
        while let Some(item) = monkeys[i].items.pop_front() {
            let current_monkey = &mut monkeys[i];

            current_monkey.inspection_count += 1;
            let mut item = current_monkey.op.apply(item);

            if divide_by_three {
                item /= 3;
            }

            item %= global_modulus;

            let target = if item % current_monkey.divide_test == 0 {
                current_monkey.on_true
            } else {
                current_monkey.on_false
            };
            monkeys[target].items.push_back(item);
        }
    }
}

fn simulate(mut monkeys: Vec<Monkey>, nb_rounds: usize, divide_by_three: bool) -> usize {
    for _i in 0..nb_rounds {
        play_round(&mut monkeys, divide_by_three);
    }

    monkeys.sort_by(|a, b| b.inspection_count.cmp(&a.inspection_count));

    monkeys[0].inspection_count * monkeys[1].inspection_count
}

fn part1(monkeys: Vec<Monkey>) -> usize {
    simulate(monkeys, 20, true)
}

fn part2(monkeys: Vec<Monkey>) -> usize {
    simulate(monkeys, 10_000, false)
}

fn main() {
    let lines = io::stdin().lines().collect::<Result<Vec<_>, _>>().unwrap();
    let monkeys = parse_monkeys(&lines);

    println!("Part 1: {}", part1(monkeys.clone()));
    println!("Part 2: {}", part2(monkeys));
}
