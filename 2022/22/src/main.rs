use std::{
    collections::{BTreeMap, BTreeSet},
    io,
    ops::Add,
};

use nom::{branch::alt, bytes::streaming::tag, character::complete::i64, IResult};

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
struct Pos {
    x: i64,
    y: i64,
}

impl Add<Pos> for Pos {
    type Output = Pos;

    fn add(self, rhs: Pos) -> Self::Output {
        Pos {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
enum Side {
    Left,
    Right,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
enum Instruction {
    Rotate(Side),
    Advance(i64),
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, PartialOrd, Ord)]
enum Dir {
    Right = 0,
    Down,
    Left,
    Up,
}

impl Dir {
    fn rotated(&self, r: Side) -> Self {
        match (self, r) {
            (Dir::Right, Side::Left) => Dir::Up,
            (Dir::Right, Side::Right) => Dir::Down,
            (Dir::Down, Side::Left) => Dir::Right,
            (Dir::Down, Side::Right) => Dir::Left,
            (Dir::Left, Side::Left) => Dir::Down,
            (Dir::Left, Side::Right) => Dir::Up,
            (Dir::Up, Side::Left) => Dir::Left,
            (Dir::Up, Side::Right) => Dir::Right,
        }
    }

    fn delta(&self) -> Pos {
        match self {
            Dir::Right => Pos { x: 1, y: 0 },
            Dir::Down => Pos { x: 0, y: 1 },
            Dir::Left => Pos { x: -1, y: 0 },
            Dir::Up => Pos { x: 0, y: -1 },
        }
    }

    fn value(&self) -> i64 {
        *self as i64
    }

    fn symbol(&self) -> char {
        match self {
            Dir::Right => '>',
            Dir::Down => 'v',
            Dir::Left => '<',
            Dir::Up => '^',
        }
    }
}

fn parse_rotate(input: &str) -> IResult<&str, Instruction> {
    let (input, letter) = alt((tag("L"), tag("R")))(input)?;

    let dir = match letter {
        "L" => Side::Left,
        "R" => Side::Right,
        _ => panic!("Unknown rotate letter"),
    };

    Ok((input, Instruction::Rotate(dir)))
}

fn parse_advance(input: &str) -> IResult<&str, Instruction> {
    i64(input).map(|(i, n)| (i, Instruction::Advance(n)))
}

fn parse_instructions(mut input: &str) -> IResult<&str, Vec<Instruction>> {
    let mut instructions = Vec::new();

    let (new_input, instr) = parse_advance(input).unwrap();
    input = new_input;
    instructions.push(instr);
    while !input.is_empty() {
        let (new_input, instr) = parse_rotate(input).unwrap();
        input = new_input;
        instructions.push(instr);
        let (new_input, instr) = parse_advance(input).unwrap();
        input = new_input;
        instructions.push(instr);
    }

    Ok((input, instructions))
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct Range {
    from: i64,
    to: i64,
}

impl Range {
    fn contains(&self, n: i64) -> bool {
        self.from <= n && n <= self.to
    }
}

type State = (Pos, Dir);

#[derive(Debug)]
struct Map {
    walls: BTreeSet<Pos>,
    portals: BTreeMap<State, State>,
    column_ranges: BTreeMap<i64, Range>,
    line_ranges: BTreeMap<i64, Range>,
    width: i64,
    height: i64,
}

fn tore_portals(
    column_ranges: &BTreeMap<i64, Range>,
    line_ranges: &BTreeMap<i64, Range>,
) -> BTreeMap<State, State> {
    let mut portals: BTreeMap<State, State> = BTreeMap::new();

    for (&x, range) in column_ranges.iter() {
        portals.insert(
            (
                Pos {
                    x,
                    y: range.from - 1,
                },
                Dir::Up,
            ),
            (Pos { x, y: range.to }, Dir::Up),
        );
        portals.insert(
            (Pos { x, y: range.to + 1 }, Dir::Down),
            (Pos { x, y: range.from }, Dir::Down),
        );
    }

    for (&y, range) in line_ranges.iter() {
        portals.insert(
            (
                Pos {
                    x: range.from - 1,
                    y,
                },
                Dir::Left,
            ),
            (Pos { x: range.to, y }, Dir::Left),
        );
        portals.insert(
            (Pos { x: range.to + 1, y }, Dir::Right),
            (Pos { x: range.from, y }, Dir::Right),
        );
    }

    portals
}

fn parse_map(lines: impl Iterator<Item = String>) -> Map {
    let mut walls: BTreeSet<Pos> = BTreeSet::new();
    let mut line_ranges: BTreeMap<i64, Range> = BTreeMap::new();

    for (y, line) in lines.enumerate().map(|(y, l)| ((y + 1) as i64, l)) {
        let mut from = 0;
        for (x, c) in line.chars().enumerate().map(|(x, l)| ((x + 1) as i64, l)) {
            if c == '#' {
                walls.insert(Pos { x, y });
            }
            if c != ' ' && from == 0 {
                from = x;
            }
        }
        let to = line.len() as i64;
        line_ranges.insert(y, Range { from, to });
    }

    let mut column_ranges: BTreeMap<i64, Range> = BTreeMap::new();

    let width = line_ranges.iter().map(|(_, r)| r.to).max().unwrap();

    for x in 1..=width {
        let from = line_ranges
            .iter()
            .filter_map(|(y, range)| if range.contains(x) { Some(*y) } else { None })
            .min()
            .unwrap();
        let to = line_ranges
            .iter()
            .filter_map(|(y, range)| if range.contains(x) { Some(*y) } else { None })
            .max()
            .unwrap();
        column_ranges.insert(x, Range { from, to });
    }

    let portals = tore_portals(&column_ranges, &line_ranges);
    let height = column_ranges.iter().map(|(_, r)| r.to).max().unwrap();

    Map {
        walls,
        portals,
        column_ranges,
        line_ranges,
        height,
        width,
    }
}

fn execute_instruction(
    (mut current_pos, mut current_dir): State,
    instruction: &Instruction,
    map: &Map,
) -> (Pos, Dir) {
    let (new_pos, new_dir) = match *instruction {
        Instruction::Rotate(side) => (current_pos, current_dir.rotated(side)),
        Instruction::Advance(n) => {
            for _ in 0..n {
                let delta = current_dir.delta();
                let mut new_pos = current_pos + delta;
                let mut new_dir = current_dir;

                if let Some((p, d)) = map.portals.get(&(new_pos, current_dir)) {
                    new_pos = *p;
                    new_dir = *d;
                }

                if map.walls.contains(&new_pos) {
                    break;
                }

                current_pos = new_pos;
                current_dir = new_dir;
            }

            (current_pos, current_dir)
        }
    };

    // print_map(map, Some((new_pos, new_dir)));

    (new_pos, new_dir)
}

fn print_map(map: &Map, opt_current: Option<State>) {
    for y in 1..=map.height {
        let line_range = map.line_ranges[&y];
        for x in 1..=map.width {
            let current_pos = Pos { x, y };
            let c = if let Some(sym) = opt_current
                .filter(|(p, _)| *p == current_pos)
                .map(|(_, d)| d.symbol())
            {
                sym
            } else if x < line_range.from || x > line_range.to {
                ' '
            } else if map.walls.contains(&current_pos) {
                '#'
            } else {
                '.'
            };
            print!("{c}");
        }
        println!();
    }
    println!();
}

fn part1(map: &Map, instructions: &[Instruction]) -> i64 {
    let start = Pos {
        x: map.line_ranges[&1].from,
        y: 1,
    };

    let (end_pos, end_dir) = instructions
        .iter()
        .fold((start, Dir::Right), |p, d| execute_instruction(p, d, map));

    1000 * end_pos.y + 4 * end_pos.x + end_dir.value()
}

fn cube_portals_exemple(map: &Map) -> BTreeMap<State, State> {
    let n = map.height / 3;
    let mut portals: BTreeMap<State, State> = BTreeMap::new();

    // edge 1-2
    for i in 1..=n {
        portals.insert(
            (Pos { x: n * 2 + i, y: 0 }, Dir::Up),
            (
                Pos {
                    x: n - i + 1,
                    y: n + 1,
                },
                Dir::Down,
            ),
        );
        portals.insert(
            (Pos { x: n - i + 1, y: n }, Dir::Up),
            (Pos { x: 2 * n + i, y: 1 }, Dir::Down),
        );
    }

    // edge 1-3
    for i in 1..=n {
        portals.insert(
            (Pos { x: 2 * n, y: i }, Dir::Left),
            (Pos { x: n + i, y: n + 1 }, Dir::Down),
        );
        portals.insert(
            (Pos { x: n + i, y: n }, Dir::Up),
            (Pos { x: 2 * n + 1, y: i }, Dir::Right),
        );
    }

    // edge 1-6
    for i in 1..=n {
        portals.insert(
            (Pos { x: 3 * n + 1, y: i }, Dir::Right),
            (
                Pos {
                    x: 4 * n,
                    y: 3 * n - i + 1,
                },
                Dir::Left,
            ),
        );
        portals.insert(
            (
                Pos {
                    x: 4 * n + 1,
                    y: 3 * n - i + 1,
                },
                Dir::Right,
            ),
            (Pos { x: 3 * n, y: i }, Dir::Left),
        );
    }

    // edge 4-6
    for i in 1..=n {
        portals.insert(
            (
                Pos {
                    x: 3 * n + 1,
                    y: n + i,
                },
                Dir::Right,
            ),
            (
                Pos {
                    x: 4 * n - i + 1,
                    y: 2 * n + 1,
                },
                Dir::Down,
            ),
        );
        portals.insert(
            (
                Pos {
                    x: 4 * n - i + 1,
                    y: 2 * n,
                },
                Dir::Up,
            ),
            (Pos { x: 3 * n, y: n + i }, Dir::Left),
        );
    }

    // edge 3-5
    for i in 1..=n {
        portals.insert(
            (
                Pos {
                    x: n + i,
                    y: 2 * n + 1,
                },
                Dir::Down,
            ),
            (
                Pos {
                    x: 2 * n + 1,
                    y: 3 * n - i + 1,
                },
                Dir::Right,
            ),
        );
        portals.insert(
            (
                Pos {
                    x: 2 * n,
                    y: 3 * n - i + 1,
                },
                Dir::Left,
            ),
            (Pos { x: n + i, y: 2 * n }, Dir::Up),
        );
    }

    // edge 2-5
    for i in 1..=n {
        portals.insert(
            (Pos { x: i, y: 2 * n + 1 }, Dir::Down),
            (
                Pos {
                    x: 3 * n - i + 1,
                    y: 3 * n,
                },
                Dir::Up,
            ),
        );
        portals.insert(
            (
                Pos {
                    x: 3 * n - i + 1,
                    y: 3 * n + 1,
                },
                Dir::Down,
            ),
            (Pos { x: i, y: 2 * n }, Dir::Up),
        );
    }

    // edge 2-6
    for i in 1..=n {
        portals.insert(
            (Pos { x: 0, y: n + i }, Dir::Left),
            (
                Pos {
                    x: 4 * n - i + 1,
                    y: 3 * n,
                },
                Dir::Up,
            ),
        );
        portals.insert(
            (
                Pos {
                    x: 4 * n - i + 1,
                    y: 3 * n + 1,
                },
                Dir::Down,
            ),
            (Pos { x: 1, y: n + i }, Dir::Right),
        );
    }

    portals
}

fn cube_portals_input(map: &Map) -> BTreeMap<State, State> {
    let n = map.height / 4;
    let mut portals: BTreeMap<State, State> = BTreeMap::new();

    // edge 1-4
    for i in 1..=n {
        portals.insert(
            (Pos { x: n, y: i }, Dir::Left),
            (
                Pos {
                    x: 1,
                    y: 3 * n - i + 1,
                },
                Dir::Right,
            ),
        );
        portals.insert(
            (
                Pos {
                    x: 0,
                    y: 3 * n - i + 1,
                },
                Dir::Left,
            ),
            (Pos { x: n + 1, y: i }, Dir::Right),
        );
    }

    // edge 3-4
    for i in 1..=n {
        portals.insert(
            (Pos { x: n, y: n + i }, Dir::Left),
            (Pos { x: i, y: 2 * n + 1 }, Dir::Down),
        );
        portals.insert(
            (Pos { x: i, y: 2 * n }, Dir::Up),
            (Pos { x: n + 1, y: n + i }, Dir::Right),
        );
    }

    // edge 1-6
    for i in 1..=n {
        portals.insert(
            (Pos { x: n + i, y: 0 }, Dir::Up),
            (Pos { x: 1, y: 3 * n + i }, Dir::Right),
        );
        portals.insert(
            (Pos { x: 0, y: 3 * n + i }, Dir::Left),
            (Pos { x: n + i, y: 1 }, Dir::Down),
        );
    }

    // edge 2-3
    for i in 1..=n {
        portals.insert(
            (
                Pos {
                    x: 2 * n + i,
                    y: n + 1,
                },
                Dir::Down,
            ),
            (Pos { x: 2 * n, y: n + i }, Dir::Left),
        );
        portals.insert(
            (
                Pos {
                    x: 2 * n + 1,
                    y: n + i,
                },
                Dir::Right,
            ),
            (Pos { x: 2 * n + i, y: n }, Dir::Up),
        );
    }

    // edge 2-5
    for i in 1..=n {
        portals.insert(
            (Pos { x: 3 * n + 1, y: i }, Dir::Right),
            (
                Pos {
                    x: 2 * n,
                    y: 3 * n - i + 1,
                },
                Dir::Left,
            ),
        );
        portals.insert(
            (
                Pos {
                    x: 2 * n + 1,
                    y: 3 * n - i + 1,
                },
                Dir::Right,
            ),
            (Pos { x: 3 * n, y: i }, Dir::Left),
        );
    }

    // edge 5-6
    for i in 1..=n {
        portals.insert(
            (
                Pos {
                    x: n + 1,
                    y: 3 * n + i,
                },
                Dir::Right,
            ),
            (Pos { x: n + i, y: 3 * n }, Dir::Up),
        );
        portals.insert(
            (
                Pos {
                    x: n + i,
                    y: 3 * n + 1,
                },
                Dir::Down,
            ),
            (Pos { x: n, y: 3 * n + i }, Dir::Left),
        );
    }

    // edge 2-6
    for i in 1..=n {
        portals.insert(
            (Pos { x: 2 * n + i, y: 0 }, Dir::Up),
            (Pos { x: i, y: 4 * n }, Dir::Up),
        );
        portals.insert(
            (Pos { x: i, y: 4 * n + 1 }, Dir::Down),
            (Pos { x: 2 * n + i, y: 1 }, Dir::Down),
        );
    }
    portals
}

fn part2(mut map: Map, instructions: &[Instruction]) -> i64 {
    // map.portals = cube_portals_exemple(&map);
    map.portals = cube_portals_input(&map);

    let start = Pos {
        x: map.line_ranges[&1].from,
        y: 1,
    };

    let (end_pos, end_dir) = instructions
        .iter()
        .fold((start, Dir::Right), |p, d| execute_instruction(p, d, &map));

    1000 * end_pos.y + 4 * end_pos.x + end_dir.value()
}

fn main() {
    let lines = io::stdin()
        .lines()
        .collect::<Result<Vec<String>, _>>()
        .unwrap();

    let instructions: Vec<Instruction> = parse_instructions(&lines.last().unwrap()).unwrap().1;

    let map: Map = parse_map(lines.into_iter().take_while(|s| s != ""));

    // print_map(&map, None);

    println!("Part 1: {}", part1(&map, &instructions));
    println!("Part 2: {}", part2(map, &instructions)); // /!\ change the portals generation if the input changes
}
