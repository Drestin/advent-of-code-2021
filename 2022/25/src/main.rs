use std::io;

type SnafuNumber = Vec<char>;

fn snafu_to_normal(snafu: &SnafuNumber) -> i64 {
    snafu
        .iter()
        .rev()
        .enumerate()
        .map(|(i, c)| match c {
            '=' => -2,
            '-' => -1,
            '0' => 0,
            '1' => 1,
            '2' => 2,
            _ => panic!("Unknown char '{c}'"),
        } * 5_i64.pow(i as u32))
        .sum()
}

fn normal_to_snafu(input_n: i64) -> SnafuNumber {
    let mut n = input_n;
    let mut vec: Vec<char> = Vec::new();
    while n > 0 {
        let c = match n % 5 {
            0 => '0',
            1 => '1',
            2 => '2',
            3 => {
                n += 2;
                '='
            }
            4 => {
                n += 1;
                '-'
            }
            _ => panic!("Failed to convert {input_n} to SNAFU"),
        };

        vec.push(c);
        n /= 5;
    }

    if vec.is_empty() {
        vec!['0']
    } else {
        vec.reverse();
        vec
    }
}

fn main() {
    let snafu_numbers: Vec<SnafuNumber> = io::stdin()
        .lines()
        .map(|l| l.unwrap().chars().collect())
        .collect();

    let sum: i64 = snafu_numbers
        .iter()
        .map(|snafu| snafu_to_normal(snafu))
        .sum();

    let sum_snafu = normal_to_snafu(sum);

    println!("Part 1: {}", sum_snafu.iter().collect::<String>());
}
