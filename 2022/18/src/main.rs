use std::{
    collections::{BTreeSet, VecDeque},
    io,
    ops::Add,
};

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone, Copy)]
struct Pos {
    x: i32,
    y: i32,
    z: i32,
}

impl Add<&Pos> for &Pos {
    type Output = Pos;

    fn add(self, rhs: &Pos) -> Self::Output {
        Pos {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl Pos {
    const SIDES_DELTAS: [Pos; 6] = [
        Pos { x: 1, y: 0, z: 0 },
        Pos { x: -1, y: 0, z: 0 },
        Pos { x: 0, y: 1, z: 0 },
        Pos { x: 0, y: -1, z: 0 },
        Pos { x: 0, y: 0, z: 1 },
        Pos { x: 0, y: 0, z: -1 },
    ];
}

fn part1(cubes: &BTreeSet<Pos>) -> usize {
    cubes
        .iter()
        .map(|pos| {
            Pos::SIDES_DELTAS
                .iter()
                .filter_map(|d| {
                    let side = pos + d;
                    if cubes.contains(&side) {
                        None
                    } else {
                        Some(())
                    }
                })
                .count()
        })
        .sum()
}

fn part2(cubes: &BTreeSet<Pos>) -> usize {
    let mut extern_surface: usize = 0;

    let mut blacks: BTreeSet<Pos> = BTreeSet::new();
    let mut greys: VecDeque<Pos> = VecDeque::new();

    let x_max: i32 = cubes.iter().map(|p| p.x).max().unwrap() + 1;
    let y_max: i32 = cubes.iter().map(|p| p.y).max().unwrap() + 1;
    let z_max: i32 = cubes.iter().map(|p| p.z).max().unwrap() + 1;

    let start_pos = Pos {
        x: -1,
        y: -1,
        z: -1,
    };
    greys.push_back(start_pos.clone());
    blacks.insert(start_pos);

    while let Some(current_pos) = greys.pop_front() {
        for side in Pos::SIDES_DELTAS
            .iter()
            .map(|d| d + &current_pos)
            .filter(|p| {
                p.x >= -1 && p.x <= x_max && p.y >= -1 && p.y <= y_max && p.z >= -1 && p.z <= z_max
            })
        {
            let is_lava = cubes.contains(&side);

            if is_lava {
                extern_surface += 1;
            }
            if !blacks.contains(&side) {
                if !is_lava {
                    greys.push_back(side.clone());
                }
                blacks.insert(side);
            }
        }
    }

    extern_surface
}

fn main() {
    let cubes: BTreeSet<Pos> = io::stdin()
        .lines()
        .map(|l| {
            let vec: Vec<i32> = l.unwrap().split(",").map(|n| n.parse().unwrap()).collect();

            Pos {
                x: vec[0],
                y: vec[1],
                z: vec[2],
            }
        })
        .collect();

    println!("Part 1: {}", part1(&cubes));
    println!("Part 2: {}", part2(&cubes));
}
