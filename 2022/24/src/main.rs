use std::{
    collections::{BTreeMap, BTreeSet},
    io,
    ops::{Add, Mul},
};

use priority_queue::PriorityQueue;

#[derive(PartialEq, Eq, Clone, Copy, PartialOrd, Ord, Debug)]
enum Dir {
    Up,
    Right,
    Down,
    Left,
}

impl Dir {
    fn delta(&self) -> Pos {
        match self {
            Dir::Up => Pos { x: 0, y: -1 },
            Dir::Down => Pos { x: 0, y: 1 },
            Dir::Left => Pos { x: -1, y: 0 },
            Dir::Right => Pos { x: 1, y: 0 },
        }
    }

    fn symbol(&self) -> char {
        match self {
            Dir::Up => '^',
            Dir::Right => '>',
            Dir::Down => 'v',
            Dir::Left => '<',
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug, Hash)]
struct Pos {
    x: i64,
    y: i64,
}

impl Add<Pos> for Pos {
    type Output = Pos;

    fn add(self, rhs: Pos) -> Self::Output {
        Pos {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Mul<Pos> for i64 {
    type Output = Pos;

    fn mul(self, pos: Pos) -> Self::Output {
        Pos {
            x: self * pos.x,
            y: self * pos.y,
        }
    }
}

impl Pos {
    const AROUND_DELTAS: [Pos; 4] = [
        Pos { x: 1, y: 0 },
        Pos { x: -1, y: 0 },
        Pos { x: 0, y: 1 },
        Pos { x: 0, y: -1 },
    ];

    fn wrap_around(&self, map: &Map) -> Self {
        let new_x = (self.x - 1).rem_euclid(map.width - 2) + 1;
        let new_y = (self.y - 1).rem_euclid(map.height - 2) + 1;

        Pos { x: new_x, y: new_y }
    }

    fn distance(&self, other: &Pos) -> i64 {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }

    fn is_in_map(&self, map: &Map) -> bool {
        *self == map.start_pos
            || *self == map.end_pos
            || (self.x >= 1 && self.x <= map.width - 2 && self.y >= 1 && self.y <= map.height - 2)
    }

    fn around_and_here<'a>(&'a self, map: &'a Map) -> impl Iterator<Item = Self> + 'a {
        Self::AROUND_DELTAS
            .iter()
            .map(|d| *self + *d)
            .filter(|p| p.is_in_map(map))
            .chain([*self].into_iter())
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
struct Blizzard {
    start_pos: Pos,
    dir: Dir,
}

impl Blizzard {
    fn pos_at_time(&self, map: &Map, time: i64) -> Pos {
        (self.start_pos + (time * self.dir.delta())).wrap_around(map)
    }
}

#[derive(Debug)]
struct Map {
    start_pos: Pos,
    end_pos: Pos,
    width: i64,
    height: i64,
    blizzards: Vec<Blizzard>,
}

fn parse_map() -> Map {
    let grid: Vec<Vec<char>> = io::stdin()
        .lines()
        .map(|l| l.unwrap().chars().collect())
        .collect();

    let start_x = grid[0].iter().position(|&c| c == '.').unwrap() as i64;
    let end_x = grid.last().unwrap().iter().position(|&c| c == '.').unwrap() as i64;

    let width = grid[0].len() as i64;
    let height = grid.len() as i64;

    let blizzards: Vec<Blizzard> = grid
        .iter()
        .enumerate()
        .flat_map(|(y, l)| {
            l.iter()
                .enumerate()
                .filter_map(|(x, c)| {
                    let start_pos = Pos {
                        x: x as i64,
                        y: y as i64,
                    };
                    let opt_dir = match c {
                        '>' => Some(Dir::Right),
                        '<' => Some(Dir::Left),
                        'v' => Some(Dir::Down),
                        '^' => Some(Dir::Up),
                        _ => None,
                    };

                    opt_dir.map(|dir| Blizzard { start_pos, dir })
                })
                .collect::<Vec<_>>()
        })
        .collect();

    Map {
        blizzards,
        start_pos: Pos { x: start_x, y: 0 },
        end_pos: Pos {
            x: end_x,
            y: height - 1,
        },
        width,
        height,
    }
}

fn print_map(map: &Map, time: i64) {
    let mut blizzards_pos: BTreeMap<Pos, Vec<Dir>> = BTreeMap::new();

    for b in &map.blizzards {
        blizzards_pos
            .entry(b.pos_at_time(map, time))
            .or_default()
            .push(b.dir);
    }

    for y in 0..map.height {
        for x in 0..map.width {
            let c = if x == 0
                || x == map.width - 1
                || (y == 0 && x != map.start_pos.x)
                || (y == map.height - 1 && x != map.end_pos.x)
            {
                '#'
            } else if let Some(dirs) = blizzards_pos.get(&Pos { x, y }) {
                if dirs.len() == 1 {
                    dirs[0].symbol()
                } else {
                    dirs.len().to_string().chars().next().unwrap()
                }
            } else {
                '.'
            };
            print!("{c}");
        }
        println!();
    }
    println!();
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug, Hash)]
struct State {
    time: i64,
    expedition_pos: Pos,
    reached_end: bool,
    re_reached_start: bool,
}

impl State {
    fn priority(self, map: &Map, need_snacks: bool) -> i64 {
        i64::MAX - self.min_time_possible(map, need_snacks)
    }

    fn min_time_possible(&self, map: &Map, need_snacks: bool) -> i64 {
        let remainding_time = if !need_snacks || self.re_reached_start {
            self.expedition_pos.distance(&map.end_pos)
        } else if self.reached_end {
            map.start_pos.distance(&map.end_pos) + self.expedition_pos.distance(&map.start_pos)
        } else {
            2 * map.start_pos.distance(&map.end_pos) + self.expedition_pos.distance(&map.end_pos)
        };

        self.time + remainding_time
    }

    fn possible_next_states<'a>(
        &'a self,
        map: &'a Map,
        cache: &'a mut BTreeMap<i64, BTreeSet<Pos>>,
    ) -> impl Iterator<Item = State> + 'a {
        let new_time = self.time + 1;
        let entry = cache.entry(new_time).or_default();
        if entry.is_empty() {
            entry.extend(map.blizzards.iter().map(|b| b.pos_at_time(map, new_time)));
        }

        self.expedition_pos
            .around_and_here(map)
            .filter(|p| !entry.contains(p))
            .map(move |p| State {
                time: new_time,
                expedition_pos: p,
                reached_end: self.reached_end || p == map.end_pos,
                re_reached_start: self.re_reached_start || (self.reached_end && p == map.start_pos),
            })
    }

    fn finished(&self, map: &Map, need_snacks: bool) -> bool {
        self.expedition_pos == map.end_pos
            && (!need_snacks || (self.reached_end && self.re_reached_start))
    }
}

fn solve(map: &Map, need_snacks: bool) -> i64 {
    let mut greys: PriorityQueue<State, i64> = PriorityQueue::new();
    let mut blacks: BTreeSet<State> = BTreeSet::new();

    let mut min_time: i64 = i64::MAX;

    {
        let start_state = State {
            time: 0,
            expedition_pos: map.start_pos,
            re_reached_start: false,
            reached_end: false,
        };
        greys.push(start_state, start_state.priority(map, need_snacks));
        blacks.insert(start_state);
    }

    let mut cache: BTreeMap<i64, BTreeSet<Pos>> = BTreeMap::new();

    while let Some((current_state, _)) = greys.pop() {
        for next_state in current_state.possible_next_states(map, &mut cache) {
            if next_state.min_time_possible(map, need_snacks) < min_time {
                if next_state.finished(map, need_snacks) {
                    min_time = next_state.time;
                } else if !blacks.contains(&next_state) {
                    greys.push(next_state, next_state.priority(map, need_snacks));
                    blacks.insert(next_state);
                }
            }
        }
    }

    min_time
}

fn main() {
    let map = parse_map();

    println!("Part 1: {}", solve(&map, false));
    println!("Part 2: {}", solve(&map, true));
}
