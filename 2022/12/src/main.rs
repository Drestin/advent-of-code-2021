use std::{cmp::Reverse, collections::HashSet, io};

use priority_queue::PriorityQueue;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
struct Pos {
    x: usize,
    y: usize,
}

impl Pos {
    fn sides(&self, width: usize, height: usize) -> Vec<Self> {
        let mut res: Vec<Self> = Vec::with_capacity(4);

        if self.x >= 1 {
            res.push(Pos {
                x: self.x - 1,
                y: self.y,
            });
        }
        if self.y >= 1 {
            res.push(Pos {
                x: self.x,
                y: self.y - 1,
            });
        }
        if self.x < width - 1 {
            res.push(Pos {
                x: self.x + 1,
                y: self.y,
            });
        }
        if self.y < height - 1 {
            res.push(Pos {
                x: self.x,
                y: self.y + 1,
            });
        }

        res
    }
}

struct Grid {
    vec: Vec<Vec<u8>>,
    start: Pos,
    end: Pos,
}

impl Grid {
    fn width(&self) -> usize {
        self.vec[0].len()
    }

    fn height(&self) -> usize {
        self.vec.len()
    }

    fn get(&self, pos: &Pos) -> u8 {
        self.vec[pos.y][pos.x]
    }

    fn from_lines(lines: impl Iterator<Item = String>) -> Grid {
        let mut start: Option<Pos> = None;
        let mut end: Option<Pos> = None;

        let vec: Vec<Vec<u8>> = lines
            .enumerate()
            .map(|(y, l)| {
                l.chars()
                    .enumerate()
                    .map(|(x, mut c)| {
                        if c == 'S' {
                            start = Some(Pos { x, y });
                            c = 'a';
                        } else if c == 'E' {
                            end = Some(Pos { x, y });
                            c = 'z';
                        }

                        c as u8
                    })
                    .collect()
            })
            .collect();

        Grid {
            vec,
            start: start.unwrap(),
            end: end.unwrap(),
        }
    }
}

fn part1(grid: &Grid) -> usize {
    // Dijkstra
    let mut greys: PriorityQueue<Pos, Reverse<usize>> = PriorityQueue::new();
    let mut blacks: HashSet<Pos> = HashSet::new();

    greys.push(grid.start, Reverse(0));

    while let Some((pos, Reverse(d))) = greys.pop() {
        if pos == grid.end {
            return d;
        }

        blacks.insert(pos);

        for p in pos.sides(grid.width(), grid.height()) {
            if !blacks.contains(&p) && grid.get(&p) <= grid.get(&pos) + 1 {
                greys.push(p, Reverse(d + 1));
            }
        }
    }

    panic!("Failed to find a path");
}

fn part2(grid: &Grid) -> usize {
    // Dijkstra from end to 'a'

    let mut greys: PriorityQueue<Pos, Reverse<usize>> = PriorityQueue::new();
    let mut blacks: HashSet<Pos> = HashSet::new();

    greys.push(grid.end, Reverse(0));

    while let Some((pos, Reverse(d))) = greys.pop() {
        if grid.get(&pos) as char == 'a' {
            return d;
        }

        blacks.insert(pos);

        for p in pos.sides(grid.width(), grid.height()) {
            if !blacks.contains(&p) && grid.get(&pos) <= grid.get(&p) + 1 {
                greys.push(p, Reverse(d + 1));
            }
        }
    }

    panic!("Failed to find a path");
}

fn main() {
    let grid = Grid::from_lines(io::stdin().lines().map(|l| l.unwrap()));

    println!("Part 1: {}", part1(&grid));
    println!("Part 2: {}", part2(&grid));
}
