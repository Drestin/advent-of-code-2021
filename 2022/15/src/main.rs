use std::{
    collections::{BTreeSet, HashSet},
    io,
    ops::{Add, AddAssign, RangeInclusive},
};

use nom::{bytes::streaming::tag, character::complete::i64, IResult};

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
struct Pos {
    x: i64,
    y: i64,
}

impl Pos {
    fn manhattan_dist(&self, other: &Pos) -> i64 {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
}

impl Add<Pos> for Pos {
    type Output = Pos;

    fn add(self, rhs: Pos) -> Self::Output {
        Pos {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl AddAssign<Pos> for Pos {
    fn add_assign(&mut self, rhs: Pos) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

#[derive(Debug)]
struct Sensor {
    pos: Pos,
    distance_to_beacon: i64,
}

impl Range {
    fn remove_range(&self, other: &Range) -> Vec<Range> {
        let mut parts = Vec::new();

        if self.from < other.from {
            parts.push(Range {
                from: self.from,
                to: self.to.min(other.from - 1),
            });
        }
        if other.to < self.to {
            parts.push(Range {
                from: self.from.max(other.to + 1),
                to: self.to,
            });
        }

        parts
    }
}

impl IntoIterator for Range {
    type Item = i64;

    type IntoIter = RangeInclusive<i64>;

    fn into_iter(self) -> Self::IntoIter {
        self.from..=self.to
    }
}
#[derive(Debug)]
struct Range {
    from: i64,
    to: i64,
}

impl Sensor {
    fn covered_range_for_y(&self, y: i64) -> Option<Range> {
        let dy = (self.pos.y - y).abs();

        if self.distance_to_beacon < dy {
            None
        } else {
            let a = self.distance_to_beacon - dy;

            Some(Range {
                from: self.pos.x - a,
                to: self.pos.x + a,
            })
        }
    }
}

#[derive(Debug)]
struct Area {
    known_beacons: HashSet<Pos>,
    sensors: Vec<Sensor>,
}

fn sensor(input: &str) -> IResult<&str, (Pos, Pos)> {
    let (input, _) = tag("Sensor at x=")(input)?;
    let (input, sensor_x) = i64(input)?;
    let (input, _) = tag(", y=")(input)?;
    let (input, sensor_y) = i64(input)?;
    let (input, _) = tag(": closest beacon is at x=")(input)?;
    let (input, beacon_x) = i64(input)?;
    let (input, _) = tag(", y=")(input)?;
    let (input, beacon_y) = i64(input)?;

    Ok((
        input,
        (
            Pos {
                x: sensor_x,
                y: sensor_y,
            },
            Pos {
                x: beacon_x,
                y: beacon_y,
            },
        ),
    ))
}

impl Area {
    fn from_lines(lines: impl Iterator<Item = String>) -> Area {
        let mut beacons: HashSet<Pos> = HashSet::new();

        let sensors = lines
            .map(|s| {
                let (sensor_pos, beacon_pos) = sensor(&s).unwrap().1;

                beacons.insert(beacon_pos);
                Sensor {
                    pos: sensor_pos,
                    distance_to_beacon: sensor_pos.manhattan_dist(&beacon_pos),
                }
            })
            .collect::<Vec<_>>();

        Area {
            known_beacons: beacons,
            sensors,
        }
    }
}

fn part1(area: &Area, y: i64) -> usize {
    let covered: usize = area
        .sensors
        .iter()
        .filter_map(|s| s.covered_range_for_y(y))
        .flat_map(|r| r.into_iter())
        .collect::<BTreeSet<_>>()
        .len();
    let beacons = area.known_beacons.iter().filter(|b| b.y == y).count();

    covered - beacons
}

fn part2(area: &Area, max_coord: i64) -> i64 {
    for y in 0..=max_coord {
        let mut ranges = vec![Range {
            from: 0,
            to: max_coord,
        }];

        for s in &area.sensors {
            if let Some(covered) = s.covered_range_for_y(y) {
                ranges = ranges
                    .iter()
                    .flat_map(|r| r.remove_range(&covered).into_iter())
                    .collect::<Vec<_>>();
            }
        }

        if ranges.len() > 0 {
            let x = ranges[0].from;
            return 4_000_000 * x + y;
        }
    }

    panic!("Could not find a solution");
}

fn main() {
    let area = Area::from_lines(io::stdin().lines().map(|l| l.unwrap()));

    println!("Part 1: {}", part1(&area, 2_000_000)); // 10 for example
    println!("Part 2: {}", part2(&area, 4_000_000)); // 20 for example
}
