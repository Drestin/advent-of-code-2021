use std::{collections::HashSet, io};

fn find_start_of_different_sequence(input: &str, size: usize) -> usize {
    input
        .as_bytes()
        .windows(size)
        .enumerate()
        .find_map(|(i, array)| {
            if array.iter().collect::<HashSet<_>>().len() == size {
                Some(i + size)
            } else {
                None
            }
        })
        .unwrap()
}

fn part1(input: &str) -> usize {
    find_start_of_different_sequence(input, 4)
}

fn part2(input: &str) -> usize {
    find_start_of_different_sequence(input, 14)
}

fn main() {
    let input = io::stdin().lines().next().unwrap().unwrap();

    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
}
