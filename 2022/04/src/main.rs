use std::{io, str::FromStr};

#[derive(Clone)]
struct Section {
    start: usize,
    end: usize,
}

impl Section {
    fn contains(&self, other: &Self) -> bool {
        self.start <= other.start && self.end >= other.end
    }

    fn overlaps(&self, other: &Self) -> bool {
        self.start <= other.end && self.end >= other.start
    }
}

impl FromStr for Section {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts = s
            .split("-")
            .map(|n| n.parse::<usize>())
            .collect::<Result<Vec<_>, _>>()
            .map_err(|err| err.to_string())?;

        Ok(Section {
            start: parts[0],
            end: parts[1],
        })
    }
}

fn part1(input: &[(Section, Section)]) -> usize {
    input
        .iter()
        .filter(|(a, b)| a.contains(b) || b.contains(a))
        .count()
}

fn part2(input: &[(Section, Section)]) -> usize {
    input.iter().filter(|(a, b)| a.overlaps(b)).count()
}

fn main() {
    let input: Vec<(Section, Section)> = io::stdin()
        .lines()
        .map(|line| {
            let sections = line
                .unwrap()
                .split(",")
                .map(|s| s.parse::<Section>().unwrap())
                .collect::<Vec<_>>();

            (sections[0].clone(), sections[1].clone())
        })
        .collect();

    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
}
