use std::io;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct Id(usize);

type Element = (Id, i64);

fn mix(initial_order: &[Element], vec: &mut Vec<Element>) {
    for &elem in initial_order.iter() {
        let n = elem.1;
        let old_index = vec.iter().position(|&x| x == elem).unwrap();
        vec.remove(old_index);
        let new_index = (old_index as i64 + n).rem_euclid(initial_order.len() as i64 - 1);
        vec.insert(new_index as usize, elem);
    }
}

fn grove_score(vec: &[Element]) -> i64 {
    let length = vec.len();
    let zero_index = vec.iter().position(|(_id, n)| *n == 0).unwrap();
    vec[(zero_index + 1000) % length].1
        + vec[(zero_index + 2000) % length].1
        + vec[(zero_index + 3000) % length].1
}

fn part1(input: &[Element]) -> i64 {
    let mut vec: Vec<Element> = input.iter().copied().collect();

    mix(input, &mut vec);

    grove_score(&vec)
}

fn part2(input: &[Element]) -> i64 {
    let decryption_key = 811589153;
    let input: Vec<Element> = input
        .iter()
        .map(|(id, n)| (*id, n * decryption_key))
        .collect();

    let mut vec: Vec<Element> = input.clone();
    for _ in 0..10 {
        mix(&input, &mut vec);
    }

    grove_score(&vec)
}

fn main() {
    let input: Vec<Element> = io::stdin()
        .lines()
        .enumerate()
        .map(|(i, l)| {
            let n = l.unwrap().parse::<i64>().unwrap();
            (Id(i), n)
        })
        .collect();

    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
}
