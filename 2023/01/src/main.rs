use std::io;

fn _part1() {
    let result = io::stdin().lines().fold(0, |sum, line| {
        let numbers: Vec<_> = line
            .unwrap()
            .chars()
            .filter(|c| c.is_digit(10))
            .map(|c| c.to_digit(10).unwrap())
            .collect();

        sum + numbers.first().unwrap() * 10 + numbers.last().unwrap()
    });

    println!("{}", result);
}

fn main() {
    let result = io::stdin().lines().fold(0, |mut sum, line| {
        let line = line.unwrap();

        for i in 0..line.len() {
            if let Some(n) = find_digit_at_start(&line[i..]) {
                sum += n * 10;
                print!("{}", n);
                break;
            }
        }

        for i in (0..line.len()).rev() {
            if let Some(n) = find_digit_at_start(&line[i..]) {
                sum += n;
                println!("{}", n);
                break;
            }
        }

        sum
    });

    println!("{}", result);
}

const DIGIT_WORDS: [(&str, u32); 18] = [
    ("one", 1),
    ("two", 2),
    ("three", 3),
    ("four", 4),
    ("five", 5),
    ("six", 6),
    ("seven", 7),
    ("eight", 8),
    ("nine", 9),
    ("1", 1),
    ("2", 2),
    ("3", 3),
    ("4", 4),
    ("5", 5),
    ("6", 6),
    ("7", 7),
    ("8", 8),
    ("9", 9),
];

fn find_digit_at_start(text: &str) -> Option<u32> {
    for (word, digit) in DIGIT_WORDS {
        if text.starts_with(word) {
            return Some(digit);
        }
    }

    None
}
