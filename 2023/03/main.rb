
def main
  input = $stdin.read.lines.map! { |l| l.chomp! }
  # input = input[5..5]

  width = input[0].length
  height = input.length

  possible_gears = []
  part_numbers = []

  for y in 0...height
    current_number = 0
    is_part = false

    for x in 0...width
      c = input[y][x]

      if is_digit?(c)
        current_number *= 10
        current_number += c.to_i

        if !is_part && is_symbol_around?(x,y,width,height,input)
          is_part = true
        end
        if is_part && (x == width - 1 || !is_digit?(input[y][x+1]))
          part_numbers << [current_number, [x - number_length(current_number) + 1,y]]

          current_number = 0
          is_part = false
        end
      else
        current_number = 0
        is_part = false

        if c == '*'
          possible_gears << [x,y]
        end
      end
    end
  end

  puts "Part 1: #{part_numbers.map {|n,_| n}.sum}"

  sum = 0
  for gear_x, gear_y in possible_gears
    adjacent_numbers = []
    for (n, (n_x, n_y)) in part_numbers
      n_x_max = n_x + number_length(n) - 1

      if n_x - 1 <= gear_x && gear_x <= n_x_max + 1 &&
        n_y - 1 <= gear_y && gear_y <= n_y + 1
        adjacent_numbers << n
      end
    end

    if adjacent_numbers.length == 2
      sum += adjacent_numbers[0] * adjacent_numbers[1]
    end
  end
  puts "Part 2: #{sum}"
end

def is_symbol_around?(x, y, w, h, text)
  max_x = w - 1
  max_y = h - 1

  is_symbol?((x-1).clamp(0, max_x), (y-1).clamp(0, max_y), text) ||
  is_symbol?((x-1).clamp(0, max_x), (y).clamp(0, max_y), text) ||
  is_symbol?((x-1).clamp(0, max_x), (y+1).clamp(0, max_y), text) ||
  is_symbol?((x).clamp(0, max_x), (y-1).clamp(0, max_y), text) ||
  is_symbol?((x).clamp(0, max_x), (y+1).clamp(0, max_y), text) ||
  is_symbol?((x+1).clamp(0, max_x), (y-1).clamp(0, max_y), text) ||
  is_symbol?((x+1).clamp(0, max_x), (y).clamp(0, max_y), text) ||
  is_symbol?((x+1).clamp(0, max_x), (y+1).clamp(0, max_y), text)
end

def is_symbol?(x,y,text)
  c = text[y][x]
  c != "." && !is_digit?(c)
end

def is_digit?(c)
  "0".ord <= c.ord && c.ord <= "9".ord
end

def number_length(n)
  Math::log10(n).to_i + 1
end

main()
