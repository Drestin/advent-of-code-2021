

QUANTITIES = {
  red: 12,
  green: 13,
  blue: 14,
}

def part1
  sum = 0
  while line = gets
    line = line.chomp
    game_id_str, subsets = line.split(":")
    if check_game(subsets)
      game_id = game_id_str.split(" ")[-1].to_i
      sum += game_id
    end
  end
  puts sum.to_s
end

def check_game1(subsets)
  subsets.split("; ").each do |subset|
    min_counts = { red: 0, blue: 0, green: 0 }
    subset.split(", ").each do |count_and_name|
      count, color = count_and_name.split(" ")
      count = count.to_i
      color = color.to_sym

      if count > QUANTITIES[color]
        return false
      end

    end
  end

  true
end

def part2()
  sum = 0
  while line = gets
    line = line.chomp
    _, subsets = line.split(":")

    min_counts = { red: 0, blue: 0, green: 0 }
    subsets.split("; ").each do |subset|

      subset.split(", ").each do |count_and_name|
        count, color = count_and_name.split(" ")
        count = count.to_i
        color = color.to_sym

        if count > min_counts[color]
          min_counts[color] = count
        end
      end

    end
    power = min_counts[:blue] * min_counts[:green] * min_counts[:red]
    # puts power
    sum += power
  end
  puts sum
end

part2()
